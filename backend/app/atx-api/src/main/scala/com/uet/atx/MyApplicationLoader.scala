package com.uet.atx

import play.api.{ Application, ApplicationLoader, BuiltInComponentsFromContext, LoggerConfigurator }
import play.api.ApplicationLoader.Context
import com.uet.atx.MyApplicationLoader.ATXComponents
import com.uet.atx.adapter.primary.webServices.WebModule
import com.uet.atx.adapter.secondary.databaseSqlLite.DbSqlLiteModule
import com.uet.atx.application.PortModule
import com.uet.common.fn.Concurrency.EC

class MyApplicationLoader extends ApplicationLoader {

  def load(context: Context): Application = ATXComponents(context).application
}

private object MyApplicationLoader {

  case class ATXComponents(override val context: Context) extends BuiltInComponentsFromContext(context)
      with WebModule
      with PortModule
      with DbSqlLiteModule {

    def ec: EC = executionContext

    // set up logger
    LoggerConfigurator(context.environment.classLoader).foreach {
      _.configure(context.environment, context.initialConfiguration, Map.empty)
    }
  }
}
