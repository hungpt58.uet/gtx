package com.uet.atx.adapter.primary.webServices

import com.github.plokhotnyuk.jsoniter_scala.core._
import com.github.plokhotnyuk.jsoniter_scala.macros.JsonCodecMaker
import com.uet.atx.adapter.primary.webServices.controllers.APIController.JsError
import com.uet.atx.application.port.input.employee.EmployeeDTO
import com.uet.atx.application.port.input.tag.{ TagDTO, TagTypeDTO }
import com.uet.atx.application.port.input.text.TextDTO

private[webServices] trait JsonCodecs {

  implicit val employeeCodec: JsonValueCodec[EmployeeDTO.Result]       = JsonCodecMaker.make
  implicit val employeesCodec: JsonValueCodec[Seq[EmployeeDTO.Result]] = JsonCodecMaker.make
  implicit val tagTypeCodec: JsonValueCodec[TagTypeDTO.Result]         = JsonCodecMaker.make
  implicit val tagTypesCodec: JsonValueCodec[Seq[TagTypeDTO.Result]]   = JsonCodecMaker.make
  implicit val tagsCodec: JsonValueCodec[Seq[TagDTO.Result]]           = JsonCodecMaker.make
  implicit val tagCodec: JsonValueCodec[TagDTO.Result]                 = JsonCodecMaker.make
  implicit val textCodec: JsonValueCodec[TextDTO.Result]               = JsonCodecMaker.make
  implicit val apiErrorCodec: JsonValueCodec[JsError]                  = JsonCodecMaker.make
}
