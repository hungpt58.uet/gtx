package com.uet.atx.adapter.primary.webServices

import com.softwaremill.macwire._
import com.uet.atx.adapter.primary.webServices.common.{ MyErrorHandler, MyLoggingFilter }
import com.uet.atx.adapter.primary.webServices.controllers.{ EmployeeController, HealthController }
import com.uet.atx.adapter.primary.webServices.controllers.tag.{ TagController, TagTypeController }
import com.uet.atx.application.port.input.employee.EmployeeUsecase
import com.uet.atx.application.port.input.tag.{ TagTypeUsecase, TagUsecase }
import com.uet.common.fn.Concurrency.EC
import play.api.http.HttpErrorHandler
import play.api.mvc.EssentialFilter
import play.api.routing.Router
import play.api.ContextBasedBuiltInComponents
import play.filters.HttpFiltersComponents
import play.filters.cors.CORSComponents
import play.filters.csp.CSPComponents
import play.filters.gzip.GzipFilterComponents

trait WebModule extends ContextBasedBuiltInComponents
    with HttpFiltersComponents
    with CSPComponents
    with CORSComponents
    with GzipFilterComponents {

  private val API_PREFIX = "/api/v1"

  protected implicit def ec: EC
  protected def employeeUc: EmployeeUsecase
  protected def tagTypeUc: TagTypeUsecase
  protected def tagUc: TagUsecase

  private lazy val healthController   = wire[HealthController]
  private lazy val employeeController = wire[EmployeeController].withPrefix(API_PREFIX)
  private lazy val tagController      = wire[TagController].withPrefix(API_PREFIX)
  private lazy val tagTypeController  = wire[TagTypeController].withPrefix(API_PREFIX)

  private val myHttpFilter: MyLoggingFilter = wire[MyLoggingFilter]
  private val errorHandler: MyErrorHandler  = wire[MyErrorHandler]

  override def httpFilters: Seq[EssentialFilter] = Seq(
    securityHeadersFilter,
    myHttpFilter,
    cspFilter,
    corsFilter,
    gzipFilter
  )

  override lazy val httpErrorHandler: HttpErrorHandler = errorHandler

  val router: Router =
    healthController
      .orElse(employeeController)
      .orElse(tagTypeController)
      .orElse(tagController)
}
