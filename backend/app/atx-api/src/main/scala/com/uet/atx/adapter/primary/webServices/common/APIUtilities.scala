package com.uet.atx.adapter.primary.webServices.common

import play.api.mvc.RequestHeader
import play.core.server.pekkohttp.PekkoHeadersWrapper

private[webServices] trait APIUtilities {

  protected def getHeaderInfo(request: RequestHeader): String = {
    val contentType = request.headers.toMap.getOrElse("Content-Type", Nil)
    val uri         = request.headers.asInstanceOf[PekkoHeadersWrapper].request.uri

    s"""
       |**** HEADER ****
       |Method: ${request.method}
       |URI: $uri
       |ContentType: $contentType
       |""".stripMargin
  }

}
