package com.uet.atx.adapter.primary.webServices.common

import com.github.plokhotnyuk.jsoniter_scala.core.writeToString
import com.uet.atx.adapter.primary.webServices.JsonCodecs
import com.uet.atx.adapter.primary.webServices.controllers.APIController.JsError
import play.api.Logging
import play.api.http.HttpErrorHandler
import play.api.mvc._
import play.api.mvc.Results.{ BadRequest, InternalServerError }

import scala.concurrent.Future

private[webServices] class MyErrorHandler extends HttpErrorHandler with Logging with APIUtilities with JsonCodecs {

  override def onClientError(request: RequestHeader, statusCode: Int, message: String): Future[Result] = {
    val jsError = new JsError("API or Request is wrong. Please check and try again")
    val result  = BadRequest(writeToString(jsError))

    Future.successful(result)
  }

  override def onServerError(request: RequestHeader, ex: Throwable): Future[Result] = {
    ex.printStackTrace()
    val jsError = new JsError("Unknown error happened. Please contact to admin for helping")
    val result  = InternalServerError(writeToString(jsError))

    Future.successful(result)
  }
}
