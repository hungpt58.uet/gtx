package com.uet.atx.adapter.primary.webServices.common

import com.uet.common.fn.Concurrency.EC
import org.apache.pekko.util.ByteString
import play.api.mvc.{ EssentialAction, EssentialFilter, RequestHeader, Result }
import play.api.Logging
import play.api.libs.streams.Accumulator

private[webServices] class MyLoggingFilter(implicit val ec: EC) extends EssentialFilter with Logging {

  def apply(nextFilter: EssentialAction): EssentialAction = (rh: RequestHeader) => {
    val startTime = System.currentTimeMillis

    val accumulator: Accumulator[ByteString, Result] = nextFilter(rh)

    accumulator.map { result =>
      val endTime     = System.currentTimeMillis
      val requestTime = endTime - startTime

      logger.info(s"${rh.method} ${rh.uri} took $requestTime ms and returned ${result.header.status}")
      result.withHeaders("Request-Time" -> s"$requestTime ms")
    }
  }
}
