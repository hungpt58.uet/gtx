package com.uet.atx.adapter.primary.webServices.controllers

import com.github.plokhotnyuk.jsoniter_scala.core._
import com.uet.atx.adapter.primary.webServices.JsonCodecs
import com.uet.atx.adapter.primary.webServices.controllers.APIController._
import play.api.data.FormError
import play.api.mvc.{ AbstractController, ControllerComponents, Result }
import play.api.routing.SimpleRouter

import scala.concurrent.Future

private[controllers] abstract class APIController(cc: ControllerComponents)
    extends AbstractController(cc) with JsonCodecs with SimpleRouter {

  protected def success[T](result: T)(implicit jsonCodec: JsonValueCodec[T]): Result =
    Ok(writeToString(result)).withJson

  protected def success: Result = NoContent

  protected def badRequest(message: String): Result =
    BadRequest(writeToString(JsError(Seq(message)))).withJson

  protected def asyncBadRequest(message: String): Future[Result] = Future.successful(badRequest(message).withJson)

  protected def asyncBadRequest(formErrors: Seq[FormError]): Future[Result] = Future.successful {
    val errors = formErrors.map { formError =>
      val isNotRequiredFieldCase = formError.message.nonEmpty && formError.message != "error.required"
      val errorMsg: String       = if (isNotRequiredFieldCase) formError.message else s"Key: ${formError.key} is missing"

      errorMsg
    }

    BadRequest(writeToString(JsError(errors))).withJson
  }

}

private[webServices] object APIController {

  implicit class RichAPIEncoding(result: Result) {

    def withJson = result.as("application/json")
  }

  case class JsError(errors: Seq[String]) {
    def this(msg: String) = this(Seq(msg))
  }
}
