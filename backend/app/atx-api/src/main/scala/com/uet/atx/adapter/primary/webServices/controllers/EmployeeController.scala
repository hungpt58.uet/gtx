package com.uet.atx.adapter.primary.webServices.controllers

import com.uet.atx.application.port.input.employee.EmployeeUsecase
import com.uet.common.fn.Concurrency.EC
import com.uet.common.model.ddd.Email
import play.api.mvc.{ Action, AnyContent, ControllerComponents, Result }
import play.api.routing.sird._
import play.api.routing.Router.Routes

import scala.concurrent.Future
import scala.util.Try
import scala.util.Success
import scala.util.Failure

private[webServices] class EmployeeController(
  cc:         ControllerComponents,
  employeeUc: EmployeeUsecase
)(implicit ec: EC) extends APIController(cc) {

  private def listAllEmployees: Action[AnyContent] = Action.async(_ => employeeUc.retrieveAllEmployees.map(success(_)))

  private def getEmployee(email: String): Action[AnyContent] = Action.async { _ =>
    def execute(typedEmail: Email): Future[Result] = {
      employeeUc
        .retrieveByEmail(typedEmail)
        .map(success(_))
        .recover {
          case e: NoSuchElementException => badRequest(e.getMessage)
        }
    }

    // Convert raw string into typed Email object within validate inside
    Try(Email(email)) match {
      case Success(typedEmail) => execute(typedEmail)
      case Failure(ex)         => asyncBadRequest(ex.getMessage)
    }
  }

  override def routes: Routes = {
    case GET(p"/employees")                    => listAllEmployees
    case GET(p"/employee" ? q"email=${email}") => getEmployee(email)
  }
}
