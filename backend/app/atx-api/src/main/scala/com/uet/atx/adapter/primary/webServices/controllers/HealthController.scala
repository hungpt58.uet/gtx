package com.uet.atx.adapter.primary.webServices.controllers

import play.api.mvc.ControllerComponents
import play.api.routing.Router.Routes
import play.api.routing.sird._

private[webServices] class HealthController(cc: ControllerComponents) extends APIController(cc) {

  override def routes: Routes = {
    case GET(p"/") => Action(Ok("Ok"))
  }
}
