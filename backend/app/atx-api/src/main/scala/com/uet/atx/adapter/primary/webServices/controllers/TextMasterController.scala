package com.uet.atx.adapter.primary.webServices.controllers

import com.uet.atx.adapter.primary.webServices.requests.TextRequest
import com.uet.atx.application.port.input.text.TextMasterUsecase
import com.uet.atx.domain.advertising.text.master.TextMasterId
import com.uet.common.fn.Concurrency.EC
import play.api.mvc.{ Action, AnyContent, ControllerComponents }
import play.api.routing.Router.Routes
import play.api.routing.sird._

private[webServices] class TextMasterController(
  textMasterUc: TextMasterUsecase,
  cc:           ControllerComponents
)(implicit ec: EC) extends APIController(cc) {

  def store: Action[AnyContent] = Action.async { implicit req =>
    TextRequest.createForm.bindFromRequest().fold(
      formWithError => asyncBadRequest(formWithError.errors),
      form => textMasterUc.createNew(form).map(success(_))
    )
  }

  def list(id: String, version: Option[Int]): Action[AnyContent] = ???

  def delete(ids: Set[String]): Action[AnyContent] = Action.async { _ =>
    if (ids.isEmpty) asyncBadRequest("Have no id to delete")
    else {
      textMasterUc
        .delete(ids.map(TextMasterId))
        .map(_ => success)
        .recover {
          case e: IllegalArgumentException => badRequest(e.getMessage)
          case e: IllegalStateException    => badRequest(e.getMessage)
        }
    }
  }

  def update: Action[AnyContent] = ???

  def updateStatus: Action[AnyContent] = ???

  def search: Action[AnyContent] = ???

  def listLatest: Action[AnyContent] = ???

  def checkRule: Action[AnyContent] = ???

  override def routes: Routes = {
    case POST(p"/text-masters")                    => store
    case GET(p"/text-masters")                     => list("???", Some(1))
    case DELETE(p"/text-masters" ? q_*"id=${ids}") => delete(ids.toSet)
    case PUT(p"/text-masters")                     => update
    case PUT(p"/text-masters/status")              => updateStatus
    case POST(p"/text-masters/search")             => search
    case GET(p"/text-masters/latest")              => listLatest
    case POST(p"/text-masters/regulate")           => checkRule
  }
}
