package com.uet.atx.adapter.primary.webServices.controllers.tag

import com.uet.atx.adapter.primary.webServices.controllers.APIController
import com.uet.atx.adapter.primary.webServices.requests.TagRequest
import com.uet.atx.application.port.input.tag.TagUsecase
import com.uet.common.fn.Concurrency.EC
import play.api.mvc.{ Action, AnyContent, ControllerComponents }
import play.api.routing.Router.Routes
import play.api.routing.sird._

private[webServices] class TagController(
  tagUsecase: TagUsecase,
  cc:         ControllerComponents
)(implicit ec: EC) extends APIController(cc) {

  private def create: Action[AnyContent] = Action.async { implicit req =>
    TagRequest.createForm.bindFromRequest().fold(
      formWithError => asyncBadRequest(formWithError.errors),
      form =>
        tagUsecase
          .createNew(form)
          .map(success(_))
          .recover {
            case e: NoSuchElementException => badRequest(e.getMessage)
          }
    )
  }

  private def delete: Action[AnyContent] = Action.async { implicit req =>
    TagRequest.deleteForm.bindFromRequest().fold(
      formWithError => asyncBadRequest(formWithError.errors),
      form =>
        tagUsecase
          .delete(form.ids)
          .map(_ => success)
          .recover {
            case e: NoSuchElementException => badRequest(e.getMessage)
          }
    )
  }

  private def update: Action[AnyContent] = Action.async { implicit req =>
    TagRequest.updateForm.bindFromRequest().fold(
      formWithError => asyncBadRequest(formWithError.errors),
      form =>
        tagUsecase
          .update(form)
          .map(success(_))
          .recover {
            case e: NoSuchElementException => badRequest(e.getMessage)
          }
    )
  }

  private def search: Action[AnyContent] = Action.async { implicit req =>
    TagRequest.searchForm.bindFromRequest().fold(
      formWithError => asyncBadRequest(formWithError.errors),
      form =>
        tagUsecase
          .search(form)
          .map(success(_))
          .recover {
            case e: NoSuchElementException => badRequest(e.getMessage)
          }
    )
  }

  override def routes: Routes = {
    case POST(p"/tags")        => create
    case DELETE(p"/tags")      => delete
    case PUT(p"/tags")         => update
    case POST(p"/tags/search") => search
  }
}
