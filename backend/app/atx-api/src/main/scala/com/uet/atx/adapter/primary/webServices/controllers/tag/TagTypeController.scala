package com.uet.atx.adapter.primary.webServices.controllers.tag

import com.uet.atx.adapter.primary.webServices.controllers.APIController
import com.uet.atx.adapter.primary.webServices.requests.TagTypeRequest
import com.uet.atx.application.port.input.tag.TagTypeUsecase
import com.uet.common.fn.Concurrency.EC
import play.api.mvc.{ Action, AnyContent, ControllerComponents }
import play.api.routing.Router.Routes
import play.api.routing.sird._

private[webServices] class TagTypeController(
  tagTypeUsecase: TagTypeUsecase,
  cc:             ControllerComponents
)(implicit val ec: EC) extends APIController(cc) {

  private def create: Action[AnyContent] = Action.async { implicit req =>
    TagTypeRequest.createForm.bindFromRequest().fold(
      formWithError => asyncBadRequest(formWithError.errors),
      form => tagTypeUsecase.createNew(form).map(success(_))
    )
  }

  private def search: Action[AnyContent] = Action.async { implicit req =>
    TagTypeRequest.searchForm.bindFromRequest().fold(
      formWithError => asyncBadRequest(formWithError.errors),
      form => tagTypeUsecase.search(form).map(success(_))
    )
  }

  override def routes: Routes = {
    case POST(p"/tag-types")        => create
    case POST(p"/tag-types/search") => search
  }
}
