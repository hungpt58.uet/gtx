package com.uet.atx.adapter.primary.webServices.requests

import play.api.data.validation.{ Constraint, Invalid, Valid, ValidationError }

private[requests] trait RequestForm {

  protected def nonEmptyList[T]: Constraint[Seq[T]] = Constraint[Seq[T]]("constraint.required") { o =>
    if (o.nonEmpty) Valid else Invalid(ValidationError("request wrong format"))
  }

  protected def nonDuplicated[T](ids: Seq[T]): Boolean = {
    val actualIds = ids.distinct
    ids.diff(actualIds).isEmpty
  }
}
