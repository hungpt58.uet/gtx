package com.uet.atx.adapter.primary.webServices.requests

import com.uet.atx.application.port.input.tag.TagDTO
import com.uet.atx.domain.advertising.tag.{ TagId, TagTypeId }
import com.uet.atx.domain.shared.PromotionId
import play.api.data.Form
import play.api.data.Forms._

private[webServices] object TagRequest {

  lazy val createForm: Form[TagDTO.Create] = Form(
    mapping(
      "name"        -> nonEmptyText,
      "description" -> optional(nonEmptyText),
      "tagTypeId"   -> nonEmptyText.transform(TagTypeId, (v: TagTypeId) => v.value),
      "color"       -> optional(nonEmptyText)
    )(TagDTO.Create.apply)(TagDTO.Create.unapply)
  )

  lazy val deleteForm: Form[TagDTO.Delete] = Form(
    mapping(
      "ids" -> set(nonEmptyText).transform(_.map(TagId), (v: Set[TagId]) => v.map(_.value))
    )(TagDTO.Delete.apply)(TagDTO.Delete.unapply)
      .verifying("list of ids mustn't be empty", req => req.ids.nonEmpty)
  )

  lazy val updateForm: Form[TagDTO.Update] = Form(
    mapping(
      "id"          -> nonEmptyText.transform(TagId, (v: TagId) => v.value),
      "tagTypeId"   -> nonEmptyText.transform(TagTypeId, (v: TagTypeId) => v.value),
      "name"        -> nonEmptyText,
      "description" -> optional(text),
      "color"       -> optional(text)
    )(TagDTO.Update.apply)(TagDTO.Update.unapply)
  )

  lazy val searchForm: Form[TagDTO.Search] = Form(
    mapping(
      "promotionIds" -> set(nonEmptyText).transform(_.map(PromotionId), (v: Set[PromotionId]) => v.map(_.value)),
      "name"         -> optional(nonEmptyText),
      "tagTypeId"    -> optional(nonEmptyText).transform(_.map(TagTypeId), (v: Option[TagTypeId]) => v.map(_.value))
    )(TagDTO.Search.apply)(TagDTO.Search.unapply)
      .verifying("promotionIds mustn't be empty", req => req.promotionIds.nonEmpty)
  )
}
