package com.uet.atx.adapter.primary.webServices.requests

import com.uet.atx.application.port.input.tag.TagTypeDTO
import com.uet.atx.domain.advertising.tag.TagId
import com.uet.atx.domain.shared.PromotionId
import play.api.data.Form
import play.api.data.Forms._

private[webServices] object TagTypeRequest {

  lazy val createForm: Form[TagTypeDTO.Create] = Form(
    mapping(
      "name"        -> nonEmptyText,
      "promotionId" -> nonEmptyText.transform(PromotionId, (v: PromotionId) => v.value)
    )(TagTypeDTO.Create.apply)(TagTypeDTO.Create.unapply)
  )

  lazy val searchForm: Form[TagTypeDTO.Search] = Form(
    mapping(
      "tagIds"       -> set(nonEmptyText).transform(_.map(TagId), (v: Set[TagId]) => v.map(_.value)),
      "promotionIds" -> set(nonEmptyText).transform(_.map(PromotionId), (v: Set[PromotionId]) => v.map(_.value))
    )(TagTypeDTO.Search.apply)(TagTypeDTO.Search.unapply)
  )
}
