package com.uet.atx.adapter.primary.webServices.requests

import com.uet.atx.application.port.input.text.TextDTO
import com.uet.atx.domain.advertising.label.LabelId
import com.uet.atx.domain.advertising.tag.TagTypeId
import com.uet.atx.domain.advertising.text.master.{LabeledText, TextMasterId}
import com.uet.atx.domain.shared._
import org.apache.pekko.http.scaladsl.model.DateTime
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.format.Formats._

private[webServices] object TextRequest extends RequestForm {

  lazy val createForm: Form[TextDTO.Create] = Form(
    mapping(
      "promotionId"  -> nonEmptyText.transform(PromotionId, (v: PromotionId) => v.value),
      "adFormatId"   -> nonEmptyText.transform(AdFormatId, (v: AdFormatId) => v.value),
      "labeledTexts" -> seq(mapping(
        "labelId" -> nonEmptyText.transform(LabelId, (v: LabelId) => v.value),
        "texts"   -> set(nonEmptyText)
      )(LabeledText.apply)(LabeledText.unapply)),
      "projectName"  -> optional(nonEmptyText),
      "score"        -> optional(of[Double]),
      "checkType"    -> optional(number).transform(_.map(AdFormatCheckType(_)), (v: Option[AdFormatCheckType]) => v.map(_.code)),
      "comment"      -> optional(text)
    )(TextDTO.Create.apply)(TextDTO.Create.unapply)
  )

  lazy val updateForm: Form[TextDTO.Update] = Form(
    mapping(
      "id"           -> nonEmptyText.transform(TextMasterId, (v: TextMasterId) => v.value),
      "adFormatId"   -> nonEmptyText.transform(AdFormatId, (v: AdFormatId) => v.value),
      "labeledTexts" -> seq(mapping(
        "labelId" -> nonEmptyText.transform(LabelId, (v: LabelId) => v.value),
        "texts"   -> set(nonEmptyText)
      )(LabeledText.apply)(LabeledText.unapply)),
      "projectName"  -> optional(text),
      "score"        -> optional(of[Double]),
      "checkType"    -> optional(number).transform(t => t.map(AdFormatCheckType(_)), (a: Option[AdFormatCheckType]) => a.map(_.code)),
      "comment"      -> optional(text)
    )(TextDTO.Update.apply)(TextDTO.Update.unapply)
      .verifying("labelId is duplicated", dto => nonDuplicated(dto.labeledTexts.map(_.labelId)))
      .verifying("score must be greater or equal to 0", dto => dto.score.forall(_ > 0))
  )

  lazy val changeStatusForm: Form[Seq[TextDTO.ChangeStatus]] = Form(
    seq(
      mapping(
        "id"                -> nonEmptyText.transform(TextMasterId, (v: TextMasterId) => v.value),
        "approvalStatus"    -> optional(number).transform(_.map(v => ApprovalStatus(v)()()), (v: Option[ApprovalStatus]) => v.map(_.code)),
        "submitStatus"      -> optional(number).transform(_.map(v => SubmitStatus(v)()), (v: Option[SubmitStatus]) => v.map(_.code)),
        "notApprovalReason" -> optional(nonEmptyText)
      )(TextDTO.ChangeStatus.apply)(TextDTO.ChangeStatus.unapply)
        .verifying(
          "request wrong format",
          dto => dto.approvalStatus.isDefined || dto.submitStatus.isDefined || dto.notApprovalReason.isDefined)
    ) verifying nonEmptyList
  )

  lazy val searchForm: Form[TextDTO.Search] = Form(
    mapping(
      "text"           -> optional(nonEmptyText),
      "adFormatIds"    -> set(nonEmptyText).transform(_.map(AdFormatId), (v: Set[AdFormatId]) => v.map(_.value)),
      "approvalStatus" -> set(number).transform(_.map(ApprovalStatus(_)(Some(DateTime.now))()), (v: Set[ApprovalStatus]) => v.map(_.code)),
      "promotionIds"   -> set(nonEmptyText).transform(_.map(PromotionId), (v: Set[PromotionId]) => v.map(_.value)),
      "tags"           -> set(mapping(
        "name"      -> nonEmptyText,
        "tagTypeId" -> nonEmptyText.transform(TagTypeId, (v: TagTypeId) => v.value)
      )(TextDTO.TagDTO.apply)(TextDTO.TagDTO.unapply)),
      "projectNames"   -> set(nonEmptyText),
      "textMasterIds"  -> set(nonEmptyText).transform(_.map(TextMasterId), (v: Set[TextMasterId]) => v.map(_.value))
    )(TextDTO.Search.apply)(TextDTO.Search.unapply)
      .verifying("request wrong format", promotionIds => promotionIds.promotionIds.nonEmpty)
  )

  lazy val ruleForm: Form[TextDTO.Rule] = Form(
    mapping(
      "promotionId"  -> optional(nonEmptyText).transform(_.map(PromotionId), (v: Option[PromotionId]) => v.map(_.value)),
      "adFormatId"   -> nonEmptyText.transform(AdFormatId, (v: AdFormatId) => v.value),
      "checkType"    -> optional(number).transform(_.map(AdFormatCheckType(_)), (v: Option[AdFormatCheckType]) => v.map(_.code)),
      "labeledTexts" -> seq(mapping(
        "labelId" -> nonEmptyText.transform(LabelId, (v: LabelId) => v.value),
        "texts"   -> set(nonEmptyText)
      )(LabeledText.apply)(LabeledText.unapply))
    )(TextDTO.Rule.apply)(TextDTO.Rule.unapply)
      .verifying("labelId is duplicated", dto => nonDuplicated(dto.labeledTexts.map(_.labelId)))
  )
}
