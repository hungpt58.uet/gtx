package com.uet.atx.adapter.secondary.databaseSqlLite

import com.softwaremill.macwire.wire
import com.uet.atx.adapter.secondary.databaseSqlLite.base.DbExecContext
import com.uet.atx.adapter.secondary.databaseSqlLite.repositories.assetMaster.AssetMasterRepositoryOnSqlLite
import com.uet.atx.adapter.secondary.databaseSqlLite.repositories.employee.EmployeeRepositoryOnSqlLite
import com.uet.atx.adapter.secondary.databaseSqlLite.repositories.tag.{ TagRepositoryOnSqlLite, TagTypeRepositoryOnSqlLite }
import com.uet.atx.application.port.output.EmployeeRepository
import com.uet.atx.application.port.output.asset.AssetMasterRepository
import com.uet.atx.application.port.output.tag.{ TagRepository, TagTypeRepository }
import org.apache.pekko.actor.ActorSystem

trait DbSqlLiteModule {

  protected def actorSystem: ActorSystem

  lazy implicit val dbExecContext: DbExecContext        = wire[DbExecContext]
  lazy val employeeRepository: EmployeeRepository       = wire[EmployeeRepositoryOnSqlLite]
  lazy val assetMasterRepository: AssetMasterRepository = wire[AssetMasterRepositoryOnSqlLite]
  lazy val tagTypeRepository: TagTypeRepository         = wire[TagTypeRepositoryOnSqlLite]
  lazy val tagRepository: TagRepository                 = wire[TagRepositoryOnSqlLite]
}
