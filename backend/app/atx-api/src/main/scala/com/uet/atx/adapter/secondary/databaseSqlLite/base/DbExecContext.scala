package com.uet.atx.adapter.secondary.databaseSqlLite.base

import org.apache.pekko.actor.ActorSystem
import play.api.libs.concurrent.CustomExecutionContext

private[databaseSqlLite] class DbExecContext(system: ActorSystem) extends CustomExecutionContext(system, "concurrency.db-dispatcher")
