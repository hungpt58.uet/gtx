package com.uet.atx.adapter.secondary

import io.getquill.{ SnakeCase, SqliteJdbcContext }

package object databaseSqlLite {

  lazy val ctx = new SqliteJdbcContext(SnakeCase, "db.default.quill")
}
