package com.uet.atx.adapter.secondary.databaseSqlLite.repositories

import com.uet.common.model.utilities.DBRecord
import io.getquill.{ EntityQuery, Quoted }
import com.uet.atx.adapter.secondary.databaseSqlLite.ctx
import com.uet.common.fn.IFnUtils

private[repositories] trait BaseRepository extends IFnUtils {

  import ctx._

  protected type FKStr = String

  protected def isRecordExistedByPK2[R <: DBRecord[Int, Int]](id: Int)(dao: Quoted[EntityQuery[R]]): Boolean = {
    val q = quote {
      dao
        .filter(_.id == lift(id))
        .map(_.id)
    }
    run(q.size) > 0
  }

  protected def isRecordExistedByPK[R <: DBRecord[String, String]](id: String)(dao: Quoted[EntityQuery[R]]): Boolean = {
    val q = quote {
      dao
        .filter(_.id == lift(id))
        .map(_.id)
    }
    run(q.size) > 0
  }

  protected def isRecordExistedByFK[R <: DBRecord[Int, Int]](id: Int)(dao: Quoted[EntityQuery[R]]): Boolean = {
    val q = quote {
      dao
        .filter(_.fk == lift(Option(id)))
        .map(_.id)
    }
    run(q.size) > 0
  }
}
