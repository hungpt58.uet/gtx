package com.uet.atx.adapter.secondary.databaseSqlLite.repositories.assetMaster

import com.uet.atx.adapter.secondary.databaseSqlLite.base.DbExecContext
import com.uet.atx.adapter.secondary.databaseSqlLite.ctx
import com.uet.atx.adapter.secondary.databaseSqlLite.repositories.assetMaster.records.{ AssetRecord, EntityConverter, MetadataRecord }
import com.uet.atx.adapter.secondary.databaseSqlLite.repositories.BaseRepository
import com.uet.atx.application.port.output.asset.AssetMasterRepository
import com.uet.atx.domain.advertising.asset.{ AssetMaster, AssetMasterId }
import com.uet.atx.domain.shared.{ OriginId, PromotionId }
import com.uet.common.fn.Concurrency

import scala.concurrent.Future

private[databaseSqlLite] class AssetMasterRepositoryOnSqlLite(implicit ec: DbExecContext)
    extends AssetMasterRepository with BaseRepository {

  import ctx._
  private val assetMasterDAO = quote(querySchema[AssetRecord]("asset_master_tbl"))
  private val metadataDAO    = quote(querySchema[MetadataRecord]("asset_metadata_tbl"))

  /**
   * Store an `AssetMaster` to storage. If it exists, update it and otherwise create
   *
   * @param asset The `AssetMaster` entity to be stored.
   * @return A `Future[Unit]` if the entity is stored successfully
   */
  override def store(asset: AssetMaster): Future[Unit] = Concurrency.blocking {
    val assetRecord    = AssetRecord(asset)
    val metadataRecord = MetadataRecord.ofOptional(asset)

    // -> INNER FUNCTION
    def create(): Unit = {
      transaction {
        run(quote(assetMasterDAO.insertValue(lift(assetRecord))))
        if (metadataRecord.isDefined) {
          run(quote(metadataDAO.insertValue(lift(metadataRecord.get))))
        }
      }
      ()
    }

    def update(): Unit = {
      transaction {
        run(quote(assetMasterDAO.updateValue(lift(assetRecord))))
        metadataRecord match {
          case None =>
            run(quote {
              metadataDAO.filter(_.id == lift(assetRecord.id)).delete
            })

          case Some(r) =>
            val isExisted = isRecordExistedByPK(assetRecord.id)(metadataDAO)
            if (isExisted) run(metadataDAO.updateValue(lift(r)))
            else run(metadataDAO.insertValue(lift(r)))
        }
      }
      ()
    }

    // -> START HERE
    val isExisted = isRecordExistedByPK(assetRecord.id)(assetMasterDAO)
    if (isExisted) update() else create()
  }

  /**
   * Resolves an `AssetMaster` by its identifier.
   *
   * @param id The unique identifier of the `AssetMaster` to be resolved.
   * @return A `Future[Some(v)]` if the entity is found, otherwise `Future[None]`
   */
  override def resolveById(id: AssetMasterId): Future[Option[AssetMaster]] = Concurrency.blocking {
    run(quote {
      for {
        a <- assetMasterDAO.filter(v => v.id == lift(id.value))
        m <- metadataDAO.leftJoin(v => v.id == a.id)
      } yield (a, m)
    }) headOption match {
      case Some(a -> m) => Some(EntityConverter.toEntity(a, m))
      case _            => None
    }
  }

  /**
   * Resolves a sequence of `AssetMaster` entities by their identifiers.
   *
   * @param ids A sequence of unique identifiers for the `AssetMaster` entities to be resolved.
   * @return The method returns a tuple of: (found assetMasters, notFound assetMasterIds)
   */
  override def resolveByIds(ids: Set[AssetMasterId]): Future[(Seq[AssetMaster], Set[AssetMasterId])] = Concurrency.blocking {
    val rawIds = ids.map(_.value)
    if (rawIds.isEmpty) { Seq.empty -> Set.empty }
    else {
      val foundEntities = run(quote {
        for {
          b <- assetMasterDAO.filter(a => liftQuery(rawIds).contains(a.id))
          m <- metadataDAO.leftJoin(_.id == b.id)
        } yield (b, m)
      }) map {
        case (a, m) => EntityConverter.toEntity(a, m)
      }

      val notFoundIds =
        rawIds
          .diff(foundEntities.map(_.id.value).toSet)
          .map(AssetMasterId)

      foundEntities -> notFoundIds
    }
  }

  /**
   * Resolves a sequence of `AssetMaster` entities associated with a given promotion identifier.
   *
   * @param id promotion identifier
   * @return A `Future[Seq[AssetMaster]]` that associated with the given promotion identifier
   */
  override def resolveByPromotionId(id: PromotionId): Future[Seq[AssetMaster]] = Concurrency.blocking {
    run(quote {
      for {
        b2 <- assetMasterDAO.filter(_.promotionId == lift(id.value))
        m  <- metadataDAO.leftJoin(_.id == b2.id)
      } yield (b2, m)
    }) map {
      case (a, m) => EntityConverter.toEntity(a, m)
    }
  }

  /**
   * Resolves a sequence of `AssetMaster` entities associated with a given originId
   *
   * @param id originId
   * @return A `Future[Seq[AssetMaster]]` that associated with the given originId
   */
  override def resolveByOriginId(id: OriginId): Future[Seq[AssetMaster]] = Concurrency.blocking {
    run(quote {
      for {
        a <- assetMasterDAO.filter(_.originId == lift(id.value))
        m <- metadataDAO.leftJoin(_.id == a.id)
      } yield (a, m)
    }) map {
      case (a, m) => EntityConverter.toEntity(a, m)
    }
  }

  /**
   * Resolves a map of `AssetMaster` entities associated `OriginId`
   *
   * @param ids A set of `OriginId`
   * @return A `Future[Map[OriginId, Seq[AssetMaster]]]`. Always returns all input origins as keys
   */
  override def resolveByOriginIds(ids: Set[OriginId]): Future[Map[OriginId, Seq[AssetMaster]]] = Concurrency.blocking {
    val rawIds = ids.map(_.value)
    if (rawIds.isEmpty) { Map.empty }
    else {
      val foundEntities = run(quote {
        for {
          b1 <- assetMasterDAO.filter(a => liftQuery(rawIds).contains(a.originId))
          m  <- metadataDAO.leftJoin(_.id == b1.id)
        } yield (b1, m)
      }) map {
        case (a, m) => EntityConverter.toEntity(a, m)
      }

      val groupedByOid = foundEntities.groupBy(_.idGroup.originId)
      ids.map {
        id => id -> groupedByOid.getOrElse(id, Nil)
      }.toMap
    }
  }

  /**
   * Resolves a map of `AssetMaster` entities (that is latest version) associated `OriginId`.
   *
   * @param ids A set of `OriginId`
   * @return A `Future[Map[OriginId, Seq[AssetMaster]]]`. Always returns all input origins as keys
   */
  override def resolveLatestByOriginIds(ids: Set[OriginId]): Future[Map[OriginId, Seq[AssetMaster]]] = Concurrency.blocking {
    val rawIds = ids.map(_.value)
    if (rawIds.isEmpty) { Map.empty }
    else {
      val foundEntities = run(quote {
        for {
          a <- assetMasterDAO.filter(a => liftQuery(rawIds).contains(a.originId)).filter(_.isLatest)
          m <- metadataDAO.leftJoin(_.id == a.id)
        } yield (a, m)
      }) map {
        case (a, m) => EntityConverter.toEntity(a, m)
      }

      val groupedByOid = foundEntities.groupBy(_.idGroup.originId)
      ids.map {
        id => id -> groupedByOid.getOrElse(id, Nil)
      }.toMap
    }
  }

  /**
   * This method is useful for retrieving a specific version of an `AssetMaster`
   * This allows for handling scenarios where the specified version of the `AssetMaster` does not exist.
   *
   * @param id      The unique identifier of the `AssetMaster` to be resolved.
   * @param version The specific version of the `AssetMaster` to retrieve.
   * @return A `Future[Option[AssetMaster]]` that contains the `AssetMaster` entity if found, otherwise `None`.
   */
  override def resolveByIdAndVer(id: AssetMasterId, version: Int): Future[Option[AssetMaster]] = Concurrency.blocking {
    run(quote {
      for {
        a <- assetMasterDAO.filter(_.id == lift(id.value)).filter(_.version == lift(version))
        m <- metadataDAO.leftJoin(_.id == a.id)
      } yield (a, m)
    }).headOption.map {
      case (a, m) => EntityConverter.toEntity(a, m)
    }
  }

  /**
   * Retrieves one assetMaster that is latest version linked to given originId
   *
   * @param id originId
   * @return `Future[Some(AssetMaster)]` if available, otherwise `Future[None]`
   */
  override def resolveLatestByOriginId(id: OriginId): Future[Option[AssetMaster]] = Concurrency.blocking {
    run(quote {
      for {
        a <- assetMasterDAO.filter(_.originId == lift(id.value)).filter(_.isLatest)
        m <- metadataDAO.leftJoin(_.id == a.id)
      } yield (a, m)
    }).headOption.map {
      case (a, m) => EntityConverter.toEntity(a, m)
    }
  }
}
