package com.uet.atx.adapter.secondary.databaseSqlLite.repositories.assetMaster.records

import com.uet.atx.domain.advertising.asset.AssetMaster
import com.uet.common.model.utilities.DBRecord

private[assetMaster] case class AssetRecord(
  id:                String,
  slotId:            Option[String],
  originId:          String,
  adflowId:          Option[String],
  promotionId:       String,
  axisId:            Option[String],
  originalFileName:  String,
  aliasFileName:     Option[String],
  ctxFileName:       String,
  ctxAliasFileName:  Option[String],
  projectName:       String,
  url:               Option[String],
  ytbUrl:            Option[String],
  thumbnailUrl:      Option[String],
  psdUrl:            Option[String],
  comment:           Option[String],
  approvalStatus:    String,
  notApprovalReason: Option[String],
  approvedAt:        Option[String],
  submitStatus:      String,
  tagSet:            Option[String],
  version:           Int,
  isLatest:          Boolean
) extends DBRecord[String, String] {
  override val fk: Option[String] = None
}

private[assetMaster] object AssetRecord {

  def apply(assetMaster: AssetMaster): AssetRecord =
    ???
}
