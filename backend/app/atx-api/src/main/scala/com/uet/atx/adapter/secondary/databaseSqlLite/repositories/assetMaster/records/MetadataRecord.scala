package com.uet.atx.adapter.secondary.databaseSqlLite.repositories.assetMaster.records

import com.uet.atx.domain.advertising.asset.AssetMaster
import com.uet.common.model.utilities.DBRecord

private[assetMaster] case class MetadataRecord(
  id:          String,
  fileType:    String,
  width:       Int,
  height:      Int,
  fileSize:    Long,
  extension:   String,
  duration:    Option[Double],
  audioVolume: Option[Double],
  audioCodec:  Option[String],
  videoCodec:  Option[String],
  frameRate:   Option[Double],
  bitRate:     Option[Double],
  aspectRatio: Option[Float],
  xmpTitle:    Option[String],
  hashValue:   Option[String]
) extends DBRecord[String, String] {
  override val fk: Option[String] = None
}

object MetadataRecord {
  def ofOptional(assetMaster: AssetMaster): Option[MetadataRecord] = ???
}
