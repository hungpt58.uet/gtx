package com.uet.atx.adapter.secondary.databaseSqlLite.repositories.employee

import com.uet.atx.domain.employee.{ Employee, EmployeeId }
import com.uet.common.model.ddd.Email
import com.uet.common.model.utilities.DBRecord

private[employee] case class EmployeeRecord(
  id:    String,
  email: String,
  name:  Option[String]
) extends DBRecord[String, AnyVal] {

  def this(entity: Employee) = this(entity.id.value, entity.email.value, entity.name)

  def toEntity: Employee = Employee(EmployeeId(id), Email(email), name)

  override val fk: Option[AnyVal] = None
}
