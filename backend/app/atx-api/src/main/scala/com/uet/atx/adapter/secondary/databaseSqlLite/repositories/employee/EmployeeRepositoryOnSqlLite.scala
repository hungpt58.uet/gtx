package com.uet.atx.adapter.secondary.databaseSqlLite.repositories.employee

import com.uet.atx.adapter.secondary.databaseSqlLite.base.DbExecContext
import com.uet.atx.adapter.secondary.databaseSqlLite.ctx
import com.uet.atx.application.port.output.EmployeeRepository
import com.uet.atx.domain.employee.Employee
import com.uet.common.fn.Concurrency
import com.uet.common.model.ddd.Email

import scala.concurrent.Future

private[databaseSqlLite] class EmployeeRepositoryOnSqlLite(implicit ec: DbExecContext) extends EmployeeRepository {

  import ctx._
  private val employeeDAO = quote(querySchema[EmployeeRecord]("employee_tbl"))

  /**
   * Retrieve all employees from storage
   *
   * @return List of all employees
   */
  override def resolveAll(): Future[Seq[Employee]] = Concurrency.blocking(run(employeeDAO)).map(_.map(_.toEntity))

  /**
   * Store list of employees. If employee exists, update it. Otherwise, create new
   *
   * @param employees List of employees
   * @return Nothing
   */
  override def store(employees: Seq[Employee]): Future[Unit] = Concurrency.blocking {
    run(quote {
      liftQuery(employees.map(new EmployeeRecord(_))).foreach { record =>
        employeeDAO.insertValue(record).onConflictUpdate(_.id)(
          (t, e) => t.name -> e.name,
          (t, e) => t.email -> e.email
        )
      }
    })
  }.map(_ => ())

  /**
   * Retrieve an employee by their email address.
   *
   * @param email The email address of the employee to be retrieved.
   * @return Future[None] if employee does not exist. Otherwise, return Future[Some(Employee)]
   */
  override def resolveByEmail(email: Email): Future[Option[Employee]] = Concurrency.blocking {
    run(quote {
      employeeDAO.filter(_.email == lift(email.value))
    }).map(_.toEntity).headOption
  }
}
