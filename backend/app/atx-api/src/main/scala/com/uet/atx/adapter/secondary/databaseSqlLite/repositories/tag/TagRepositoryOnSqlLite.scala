package com.uet.atx.adapter.secondary.databaseSqlLite.repositories.tag

import com.uet.atx.adapter.secondary.databaseSqlLite.base.DbExecContext
import com.uet.atx.application.port.output.tag.TagRepository
import com.uet.atx.domain.advertising.tag.{ Tag, TagId, TagTypeId }
import com.uet.atx.adapter.secondary.databaseSqlLite.ctx
import com.uet.atx.adapter.secondary.databaseSqlLite.repositories.tag.records.TagRecord
import com.uet.atx.adapter.secondary.databaseSqlLite.repositories.BaseRepository
import com.uet.common.fn.Concurrency

import scala.concurrent.Future

private[databaseSqlLite] class TagRepositoryOnSqlLite(implicit ec: DbExecContext) extends TagRepository with BaseRepository {

  import ctx._

  private val tagDAO = quote(querySchema[TagRecord]("tag_tbl"))

  /**
   * Resolves a sequence of `Tag` entities by their identifiers.
   *
   * @param ids A sequence of unique identifiers for the `Tag` entities to be resolved.
   * @return The method returns a tuple of: (found Tags, notFound tagIds)
   */
  override def resolveByIds(ids: Set[TagId]): Future[(Seq[Tag], Set[TagId])] = Concurrency.blocking {
    val rawIds = ids.map(_.value)
    if (rawIds.isEmpty) { Seq.empty -> Set.empty }
    else {
      val tags: Seq[Tag] = run(quote {
        tagDAO.filter(td => liftQuery(rawIds).contains(td.id))
      }).map(_.toEntity)
      val foundIds       = tags.map(_.id).toSet
      val notFoundIds    = ids.diff(foundIds)

      (tags, notFoundIds)
    }
  }

  /**
   * Deletes `Tag` entities identified by their unique identifiers.
   * No throw exception if id does not exist
   *
   * @param ids A set of TagIds
   * @return A `Future[Unit]` for success case
   */
  override def deleteByIds(ids: Set[TagId]): Future[Unit] = Concurrency.blocking {
    if (ids.nonEmpty) {
      val rawIds = ids.map(_.value)
      run(quote {
        tagDAO.filter(td => liftQuery(rawIds).contains(td.id)).delete
      })
    }
    ()
  }

  /**
   * Store an `Tag` to storage. If it exists, update it and otherwise create
   *
   * @param tag The `Tag` entity to be stored.
   * @return A `Future[Unit]` if the entity is stored successfully
   */
  override def store(tag: Tag): Future[Unit] = Concurrency.blocking {
    run(quote {
      tagDAO
        .insertValue(lift(new TagRecord(tag)))
        .onConflictUpdate(_.id)(
          (t, e) => t.name -> e.name,
          (t, e) => t.description -> e.description,
          (t, e) => t.tagTypeId -> e.tagTypeId,
          (t, e) => t.color -> e.color,
          (t, e) => t.isDefault -> e.isDefault
        )
    })
    ()
  }

  /**
   * Resolves a `Tag` entity by its identifier.
   * This method attempts to find a `Tag` entity corresponding to the provided identifier.
   *
   * @param id The unique identifier of the `Tag` entity to be resolved.
   * @return A `Future[Some(Tag)]` If the entity is found and otherwise `Future[None]`
   */
  override def resolveById(id: TagId): Future[Option[Tag]] = Concurrency.blocking {
    run(quote(tagDAO.filter(_.id == lift(id.value))))
      .headOption
      .map(_.toEntity)
  }

  /**
   * This method is designed to find all `Tag` entities that are associated with the specified TagType identifiers.
   * This method is always return all input ids as map-key in result
   *
   * @param ids A set of `TagTypeId`
   * @return A `Future[Map[TagTypeId, Seq[Tag]]]`
   */
  override def resolveByTagTypeIds(ids: Set[TagTypeId]): Future[Map[TagTypeId, Seq[Tag]]] = Concurrency.blocking {
    val rawIds = ids.map(_.value)
    if (rawIds.isEmpty) { Map.empty }
    else {
      val records       = run(quote(tagDAO.filter(r => liftQuery(rawIds).contains(r.tagTypeId))))
      val entityMapping = records.map(_.toEntity).groupBy(_.tagTypeId)

      ids.map(fk => fk -> entityMapping.getOrElse(fk, Nil)).toMap
    }
  }
}
