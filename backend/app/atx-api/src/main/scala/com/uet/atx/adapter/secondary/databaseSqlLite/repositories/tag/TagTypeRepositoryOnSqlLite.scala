package com.uet.atx.adapter.secondary.databaseSqlLite.repositories.tag

import com.uet.atx.adapter.secondary.databaseSqlLite.ctx
import com.uet.atx.adapter.secondary.databaseSqlLite.repositories.tag.records.TagTypeRecord
import com.uet.atx.application.port.output.tag.TagTypeRepository
import com.uet.atx.domain.advertising.tag.{ TagType, TagTypeId }
import com.uet.atx.domain.shared.PromotionId
import com.uet.common.fn.Concurrency
import com.uet.common.fn.Concurrency.EC

import scala.concurrent.Future

private[databaseSqlLite] class TagTypeRepositoryOnSqlLite(implicit ec: EC) extends TagTypeRepository {

  import ctx._
  private val tagTypeDAO = quote(querySchema[TagTypeRecord]("tag_type_tbl"))

  /**
   * Store an `TagType` to storage. If it exists, update it and otherwise create
   *
   * @param entity The `TagType` entity to be stored.
   * @return A `Future[Unit]` if the entity is stored successfully
   */
  override def store(entity: TagType): Future[Unit] = Concurrency.blocking {
    run(quote {
      tagTypeDAO
        .insertValue(lift(new TagTypeRecord(entity)))
        .onConflictUpdate(_.id)(
          (t, e) => t.name -> e.name,
          (t, e) => t.promotionId -> e.promotionId
        )
    })
    ()
  }

  /**
   * Resolves a mapping of `PromotionId` to a sequence of `TagType` entities associated with each promotion.
   *
   * @param ids A set of `PromotionId` for which the tag types are to be resolved.
   * @return A `Future[Map[OriginId, Seq[TagType]]]`. Always returns all input promotionIds as keys
   */
  override def resolveByPromotionIds(ids: Set[PromotionId]): Future[Map[PromotionId, Seq[TagType]]] = Concurrency.blocking {
    val rawIds = ids.map(_.value)
    if (rawIds.isEmpty) { Map.empty }
    else {
      val tagTypes = run(quote {
        tagTypeDAO.filter(td => td.promotionId.exists(liftQuery(rawIds).contains(_)))
      }).map(_.toEntity)

      val groupedByPromotionId = tagTypes.groupBy(_.promotionId.get) // resolve by promotionId, so _.get always safe
      ids.map(id => id -> groupedByPromotionId.getOrElse(id, Nil)).toMap
    }
  }

  /**
   * Resolves a sequence of `TagType` entities by their identifiers.
   *
   * @param ids A sequence of unique identifiers for the `TagType` entities to be resolved.
   * @return The method returns a tuple of: (found TagTypes, notFound TagTypeIds)
   */
  override def resolveByIds(ids: Set[TagTypeId]): Future[(Seq[TagType], Set[TagTypeId])] = Concurrency.blocking {
    val rawIds = ids.map(_.value)
    if (rawIds.isEmpty) { Seq.empty -> Set.empty }
    else {
      val tagTypes = run(quote {
        tagTypeDAO.filter(td => liftQuery(rawIds).contains(td.id))
      }).map(_.toEntity)

      val foundIds    = tagTypes.map(_.id)
      val notFoundIds = ids.diff(foundIds.toSet)
      (tagTypes, notFoundIds)
    }
  }

  /**
   * Resolves a `TagType` entity by its identifier.
   * This method attempts to find a `TagType` entity corresponding to the provided identifier.
   *
   * @param id The unique identifier of the `TagType` entity to be resolved.
   * @return A `Future[Some(TagType)]` If the entity is found and otherwise `Future[None]`
   */
  override def resolveById(id: TagTypeId): Future[Option[TagType]] = Concurrency.blocking {
    run(quote {
      tagTypeDAO.filter(_.id == lift(id.value))
    }).headOption.map(_.toEntity)
  }
}
