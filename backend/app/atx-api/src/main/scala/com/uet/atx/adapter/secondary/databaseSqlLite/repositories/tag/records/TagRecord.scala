package com.uet.atx.adapter.secondary.databaseSqlLite.repositories.tag.records

import com.uet.common.model.utilities.DBRecord
import com.uet.atx.domain.advertising.tag.{ Tag, TagId, TagTypeId }

private[tag] case class TagRecord(
  id:          String,
  tagTypeId:   String,
  name:        String,
  description: Option[String],
  color:       Option[String],
  isDefault:   Boolean
) extends DBRecord[String, String] {

  override val fk: Option[String] = Some(tagTypeId)

  def this(tag: Tag) = this(tag.id.value, tag.tagTypeId.value, tag.name, tag.description, tag.color, tag.isDefault)

  def toEntity: Tag = Tag(
    id          = TagId(id),
    name        = name,
    description = description,
    color       = color,
    isDefault   = isDefault,
    tagTypeId   = TagTypeId(tagTypeId))
}
