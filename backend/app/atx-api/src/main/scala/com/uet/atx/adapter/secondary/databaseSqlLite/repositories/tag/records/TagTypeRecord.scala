package com.uet.atx.adapter.secondary.databaseSqlLite.repositories.tag.records

import com.uet.atx.domain.advertising.tag.{ TagType, TagTypeId }
import com.uet.atx.domain.shared.PromotionId
import com.uet.common.model.utilities.DBRecord

private[tag] case class TagTypeRecord(
  id:          String,
  name:        String,
  promotionId: Option[String]
) extends DBRecord[String, AnyVal] {

  override val fk: Option[AnyVal] = None

  def this(entity: TagType) = this(
    entity.id.value,
    entity.name,
    entity.promotionId.map(_.value)
  )

  def toEntity: TagType = TagType(TagTypeId(id), name, promotionId.map(PromotionId))
}
