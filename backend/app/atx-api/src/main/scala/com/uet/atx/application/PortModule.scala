package com.uet.atx.application

import com.softwaremill.macwire._
import com.uet.atx.application.port.input.employee.EmployeeUsecase
import com.uet.atx.application.port.input.tag.{ TagTypeUsecase, TagUsecase }
import com.uet.atx.application.port.input.text.TextMasterUsecase
import com.uet.atx.application.port.output.{ EmployeeRepository, LabelRepository, RuleRepository, TextMasterRepository }
import com.uet.atx.application.port.output.creative.CreativeMasterRepository
import com.uet.atx.application.port.output.tag.{ TagRepository, TagTypeRepository }
import com.uet.atx.application.service.EmployeeService
import com.uet.atx.application.service.tag.{ TagService, TagTypeService }
import com.uet.atx.application.service.text.TextMasterService
import com.uet.common.fn.Concurrency.EC

trait PortModule {

  protected implicit def ec: EC
  protected def employeeRepository: EmployeeRepository
  protected def tagTypeRepository: TagTypeRepository
  protected def tagRepository: TagRepository
  protected def labelRepository: LabelRepository
  protected def textMasterRepository: TextMasterRepository
  protected def ruleRepository: RuleRepository
  protected def creativeMasterRepository: CreativeMasterRepository

  lazy val employeeUc: EmployeeUsecase     = wire[EmployeeService]
  lazy val tagTypeUc: TagTypeUsecase       = wire[TagTypeService]
  lazy val tagUc: TagUsecase               = wire[TagService]
  lazy val textMasterUc: TextMasterUsecase = wire[TextMasterService]
}
