package com.uet.atx.application.port.input.asset

import com.uet.atx.application.port.input.asset.dtos.{ AMSearchDTO, AssetMasterDTO }
import com.uet.common.model.ddd.InputPort

import scala.concurrent.Future

trait AssetMasterUsecase extends InputPort {

  /**
  * Searches for asset masters based on the given search criteria.
  *
  * @param criteria The search criteria encapsulated in `AMSearchDTO`.
  * @return A `Future` containing a sequence of `AssetMasterDTO` that match the search criteria.
  */
  def search(criteria: AMSearchDTO): Future[Seq[AssetMasterDTO]]

  /**
  * Retrieves an asset master by its raw asset ID.
  *
  * @param rawAssetId The ID of the asset to retrieve.
  * @return A `Future` containing the `AssetMasterDTO` corresponding to the specified raw asset ID.
  * @throws NoSuchElementException if don't find any asset master
  */
  def get(rawAssetId: String): Future[AssetMasterDTO]
}
