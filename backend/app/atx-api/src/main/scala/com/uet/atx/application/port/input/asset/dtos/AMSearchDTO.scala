package com.uet.atx.application.port.input.asset.dtos

import com.uet.atx.application.port.input.asset.dtos.AMSearchDTO.AMTagSearchDTO

case class AMSearchDTO(
  promotionIds:    Option[Seq[Int]],
  adflowIds:       Option[Seq[String]],
  originIds:       Option[Seq[Int]],
  approvalStatus:  Option[Seq[String]],
  submitStatus:    Option[Seq[String]],
  extensions:      Option[Seq[String]],
  pixelSizes:      Option[Seq[String]],
  aspectRatios:    Option[Seq[String]],
  projectNames:    Option[Seq[String]],
  fileNames:       Option[Seq[String]],
  assetIds:        Option[Seq[Int]],
  labelIds:        Option[Seq[Int]],
  isExactFilename: Boolean,
  tags:            Option[Seq[AMTagSearchDTO]]
)

object AMSearchDTO {
  case class AMTagSearchDTO(value: String, targetType: Option[String], typeId: Int)
}
