package com.uet.atx.application.port.input.asset.dtos

/**
 * This class is output data for almost use-cases
 */
case class AssetMasterDTO(id: Int)

object AssetMasterDTO {}
