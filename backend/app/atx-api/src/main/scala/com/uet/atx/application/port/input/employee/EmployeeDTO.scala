package com.uet.atx.application.port.input.employee

import com.uet.atx.domain.employee.Employee

object EmployeeDTO {

  case class Result(id: String, email: String) {
    def this(employee: Employee) = this(employee.id.value, employee.email.value)
  }
}
