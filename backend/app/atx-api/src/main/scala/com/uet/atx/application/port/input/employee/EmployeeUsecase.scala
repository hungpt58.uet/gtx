package com.uet.atx.application.port.input.employee

import com.uet.common.model.ddd.{ Email, InputPort }

import scala.concurrent.Future

trait EmployeeUsecase extends InputPort {

  /**
  * Retrieves a list of all employees
  */
  def retrieveAllEmployees: Future[Seq[EmployeeDTO.Result]]

  /**
  * Retrieves an employee by their email.
  *
  * @param email The email of the employee to be retrieved.
  * @return A Future containing the EmployeeDTO of the requested employee if found.
  * @throws NoSuchElementException if no employee with the given email is found
  */
  def retrieveByEmail(email: Email): Future[EmployeeDTO.Result]
}
