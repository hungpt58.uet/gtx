package com.uet.atx.application.port.input.tag

import com.uet.atx.domain.advertising.tag.{ Tag, TagId, TagTypeId }
import com.uet.atx.domain.shared.PromotionId
import com.uet.common.fn.IDGenerator

object TagDTO {

  case class Create(
    name:        String,
    description: Option[String],
    tagTypeId:   TagTypeId,
    color:       Option[String]
  ) {

    def toEntity: Tag = Tag(
      id          = TagId(IDGenerator.nextId),
      tagTypeId   = tagTypeId,
      name        = name,
      description = description,
      color       = color,
      isDefault   = false
    )
  }

  case class Delete(ids: Set[TagId])

  case class Update(
    id:          TagId,
    tagTypeId:   TagTypeId,
    name:        String,
    description: Option[String],
    color:       Option[String]
  ) {

    def toEntity: Tag = Tag(
      id          = id,
      tagTypeId   = tagTypeId,
      name        = name,
      description = description,
      color       = color,
      isDefault   = false
    )
  }

  case class Result(
    id:          String,
    name:        String,
    tagTypeId:   String,
    description: Option[String],
    color:       Option[String]
  ) {
    def this(tag: Tag) = this(tag.id.value, tag.name, tag.tagTypeId.value, tag.description, tag.color)
  }

  case class Search(promotionIds: Set[PromotionId], name: Option[String], tagTypeId: Option[TagTypeId])
}
