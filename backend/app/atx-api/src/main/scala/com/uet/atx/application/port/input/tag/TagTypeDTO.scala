package com.uet.atx.application.port.input.tag

import com.uet.atx.domain.advertising.tag.{ TagId, TagType, TagTypeId }
import com.uet.atx.domain.shared.PromotionId
import com.uet.common.fn.IDGenerator

object TagTypeDTO {

  case class Search(tagIds: Set[TagId], promotionIds: Set[PromotionId])

  case class Create(name: String, promotionId: PromotionId) {
    def toEntity: TagType = TagType(TagTypeId(IDGenerator.nextId), name, Some(promotionId))
  }

  case class Result(id: String, name: String) {
    def this(tagType: TagType) = this(tagType.id.value, tagType.name)
  }
}
