package com.uet.atx.application.port.input.tag

import com.uet.common.model.ddd.InputPort

import scala.concurrent.Future

trait TagTypeUsecase extends InputPort {

  /**
  * Stores a new TagType entity based on the provided DTO.
  *
  * @param dto contains the details of the TagType to be created.
  * @return A Future of TagTypeDTO.Result for success case.
  */
  def createNew(dto: TagTypeDTO.Create): Future[TagTypeDTO.Result]

  /**
  * Searches for TagType entities based on the criteria provided in the DTO.
  *
  * @param dto contains the search criteria for TagTypes.
  * @return A Future containing a sequence of TagTypeDTO.Result, representing the search results.
  */
  def search(dto: TagTypeDTO.Search): Future[Seq[TagTypeDTO.Result]]
}
