package com.uet.atx.application.port.input.tag

import com.uet.atx.domain.advertising.tag.TagId
import com.uet.common.model.ddd.InputPort

import scala.concurrent.Future

trait TagUsecase extends InputPort {

  /**
   * Stores a new Tag entity based on the provided DTO.
   *
   * @param dto contains the details of the Tag to be created.
   * @return A Future of new Tag
   * @throws NoSuchElementException if tagTypeId doesn't exist
   */
  def createNew(dto: TagDTO.Create): Future[TagDTO.Result]

  /**
   * Deletes the Tags identified by the given set of TagIds.
   *
   * @param ids a Set of TagId
   * @return A Future[Unit] in case successful
   * @throws NoSuchElementException if tagIds didn't find
   */
  def delete(ids: Set[TagId]): Future[Unit]

  /**
   * Updates an existing Tag entity based on the provided DTO.
   *
   * @param dto contains the updated details of the Tag.
   * @return A Future containing the updated Tag result.
   * @throws NoSuchElementException if the tagId or tagTypeId don't exist.
   */
  def update(dto: TagDTO.Update): Future[TagDTO.Result]

  /**
   * Searches for Tags based on the criteria provided in the DTO.
   *
   * @param dto the search criteria encapsulated in a TagDTO.Search object.
   * @return A Future containing a sequence of TagDTO.Result, representing the search results.
   */
  def search(dto: TagDTO.Search): Future[Seq[TagDTO.Result]]
}
