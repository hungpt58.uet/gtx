package com.uet.atx.application.port.input.text

import com.uet.atx.domain.advertising.label.Label
import com.uet.atx.domain.advertising.tag.{ Tag, TagType, TagTypeId }
import com.uet.atx.domain.advertising.text.master.{ LabeledText, TextMaster, TextMasterId }
import com.uet.atx.domain.shared.{ AdFormatCheckType, AdFormatId, ApprovalStatus, PromotionId, SubmitStatus }

object TextDTO {

  case class Create(
    promotionId:  PromotionId,
    adFormatId:   AdFormatId,
    labeledTexts: Seq[LabeledText],
    projectName:  Option[String],
    score:        Option[Double],
    checkType:    Option[AdFormatCheckType],
    comment:      Option[String]
  )

  case class Update(
    id:           TextMasterId,
    adFormatId:   AdFormatId,
    labeledTexts: Seq[LabeledText],
    projectName:  Option[String],
    score:        Option[Double],
    checkType:    Option[AdFormatCheckType],
    comment:      Option[String]
  )

  case class ChangeStatus(
    id:                TextMasterId,
    approvalStatus:    Option[ApprovalStatus],
    submitStatus:      Option[SubmitStatus],
    notApprovalReason: Option[String]
  )

  case class TagDTO(name: String, tagTypeId: TagTypeId)

  case class Search(
    text:           Option[String],
    adFormatIds:    Set[AdFormatId],
    approvalStatus: Set[ApprovalStatus],
    promotionIds:   Set[PromotionId],
    tags:           Set[TagDTO],
    projectNames:   Set[String],
    textMasterIds:  Set[TextMasterId]
  )

  case class Rule(
    promotionId:  Option[PromotionId],
    adFormatId:   AdFormatId,
    checkType:    Option[AdFormatCheckType],
    labeledTexts: Seq[LabeledText]
  )

  case class Latest(
    promotionId:    Option[PromotionId],
    approvalStatus: ApprovalStatus,
    submitStatus:   SubmitStatus
  )

  case class Result()

  def ofResults(textMasters: Seq[TextMaster], labels: Seq[Label], tags: Seq[Tag], tagTypes: Seq[TagType]): Seq[Result] = ???
}
