package com.uet.atx.application.port.input.text

import com.uet.atx.domain.advertising.text.master.TextMasterId
import com.uet.common.model.ddd.InputPort

import scala.concurrent.Future

trait TextMasterUsecase extends InputPort {

  /**
   * Creates a new TextMaster entity based on the provided DTO
   * This method first checks if the text adheres to predefined rules and if a similar TextMaster already exists.
   * If the text passes the checks and no similar TextMaster is found, a new TextMaster is created and stored.
   *
   * @param dto containing the necessary information to create a new TextMaster.
   * @return A Future containing the result DTO of the newly created or found existing TextMaster.
   * @throws IllegalStateException    If the text does not pass the predefined rules.
   * @throws IllegalArgumentException when having id is not found. Id can be labelId, tagId or tagTypeId
   */
  def createNew(dto: TextDTO.Create): Future[TextDTO.Result]

  def update(dto: TextDTO.Update): Future[Any]

  def retrieve(id: TextMasterId, version: Option[Int]): Future[Any]

  /**
   * Deletes a set of TextMaster entities identified by their IDs.
   * This method performs several checks before proceeding with the deletion:
   * - Ensures that none of the TextMasters are currently being used in any creative.
   * - Validates the existence of all specified TextMaster IDs.
   * - Checks if any of the TextMasters are in the CustomerChecking stage, which would prevent deletion.
   *
   * If any of these checks fail, an appropriate exception is thrown.
   * If all checks pass, the method proceeds to mark the TextMasters as not latest (logical deletion) by setting their `isLatest` flag to false.
   *
   * @param ids A set of TextMasterId
   * @return A Future[Unit] indicating the completion of the operation
   * @throws IllegalStateException    If any of the TextMasters are being used in creatives or are in the CustomerChecking stage.
   * @throws IllegalArgumentException If any of the specified TextMaster IDs do not exist.
   */
  def delete(ids: Set[TextMasterId]): Future[Unit]

  def search(dto: TextDTO.Search): Future[Any]

  def retrieveLatest(dto: TextDTO.Latest): Future[Any]

  def checkValidityByRule(dto: TextDTO.Rule): Future[Any]
}
