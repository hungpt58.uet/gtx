package com.uet.atx.application.port.output

import com.uet.atx.domain.employee.Employee
import com.uet.common.model.ddd.{ Email, OutputPort }

import scala.concurrent.Future

trait EmployeeRepository extends OutputPort {

  /**
   * Retrieve all employees from storage
   *
   * @return List of all employees
   */
  def resolveAll(): Future[Seq[Employee]]

  /**
   * Store list of employees. If employee exists, update it. Otherwise, create new
   *
   * @param employees List of employees
   * @return Nothing
   */
  def store(employees: Seq[Employee]): Future[Unit]

  /**
   * Retrieve an employee by their email address.
   *
   * @param email The email address of the employee to be retrieved.
   * @return Future[None] if employee does not exist. Otherwise, return Future[Some(Employee)]
   */
  def resolveByEmail(email: Email): Future[Option[Employee]]
}
