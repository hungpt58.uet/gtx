package com.uet.atx.application.port.output

import com.uet.atx.domain.advertising.label.{ Label, LabelId }
import com.uet.common.model.ddd.OutputPort

import scala.concurrent.Future

trait LabelRepository extends OutputPort {
  def resolveByIds(ids: Set[LabelId]): Future[(Seq[Label], Set[LabelId])]
}
