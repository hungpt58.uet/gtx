package com.uet.atx.application.port.output

import com.uet.atx.domain.advertising.text.master.LabeledText
import com.uet.atx.domain.shared.{ AdFormatCheckType, AdFormatId, PromotionId }
import com.uet.common.model.ddd.OutputPort

import scala.concurrent.Future

trait RuleRepository extends OutputPort {

  type ErrorMessage = String

  def checkText(
    promotionId: Option[PromotionId],
    labeledTexts: Seq[LabeledText],
    adFormatId: AdFormatId,
    checkType: Option[AdFormatCheckType]
  ): Future[Seq[ErrorMessage]]
}
