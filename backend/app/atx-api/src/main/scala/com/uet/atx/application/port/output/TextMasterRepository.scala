package com.uet.atx.application.port.output

import com.uet.atx.domain.advertising.text.master.{ TextMaster, TextMasterId }
import com.uet.atx.domain.shared.PromotionId

import scala.concurrent.Future

trait TextMasterRepository {

  // Khi store upver -> chuyen ver hien tai ve flag false
  def store(entity: TextMaster): Future[Unit]

  def store(entities: Seq[TextMaster]): Future[Unit]

  def resolveLatestByIds(ids: Set[TextMasterId]): Future[(Seq[TextMaster], Set[TextMasterId])]

  def resolveLatestByPromotionId(promotionId: PromotionId): Future[Seq[TextMaster]]

  def resolveLatestById(textId: TextMasterId): Future[Option[TextMaster]]
}
