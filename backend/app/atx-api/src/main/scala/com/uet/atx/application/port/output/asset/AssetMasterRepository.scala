package com.uet.atx.application.port.output.asset

import com.uet.atx.domain.advertising.asset.{ AssetMaster, AssetMasterId }
import com.uet.atx.domain.shared.{ OriginId, PromotionId }
import com.uet.common.model.ddd.OutputPort

import scala.concurrent.Future

trait AssetMasterRepository extends OutputPort {

  /**
   * Store an `AssetMaster` to storage. If it exists, update it and otherwise create
   *
   * @param asset The `AssetMaster` entity to be stored.
   * @return A `Future[Unit]` if the entity is stored successfully
   */
  def store(asset: AssetMaster): Future[Unit]

  /**
   * Resolves an `AssetMaster` by its identifier.
   *
   * @param id The unique identifier of the `AssetMaster` to be resolved.
   * @return A `Future[Some(v)]` if the entity is found, otherwise `Future[None]`
   */
  def resolveById(id: AssetMasterId): Future[Option[AssetMaster]]

  /**
   * Resolves a sequence of `AssetMaster` entities by their identifiers.
   *
   * @param ids A sequence of unique identifiers for the `AssetMaster` entities to be resolved.
   * @return The method returns a tuple of: (found assetMasters, notFound assetMasterIds)
   */
  def resolveByIds(ids: Set[AssetMasterId]): Future[(Seq[AssetMaster], Set[AssetMasterId])]

  /**
   * Resolves a sequence of `AssetMaster` entities associated with a given promotion identifier.
   *
   * @param id promotion identifier
   * @return A `Future[Seq[AssetMaster]]` that associated with the given promotion identifier
   */
  def resolveByPromotionId(id: PromotionId): Future[Seq[AssetMaster]]

  /**
   * Resolves a sequence of `AssetMaster` entities associated with a given originId
   *
   * @param id originId
   * @return A `Future[Seq[AssetMaster]]` that contains all versions of assetMaster
   */
  def resolveByOriginId(id: OriginId): Future[Seq[AssetMaster]]

  /**
   * Resolves a map of `AssetMaster` entities associated `OriginId`
   *
   * @param ids A set of `OriginId`
   * @return A `Future[Map[OriginId, Seq[AssetMaster]]]`. Always returns all input origins as keys
   */
  def resolveByOriginIds(ids: Set[OriginId]): Future[Map[OriginId, Seq[AssetMaster]]]

  /**
   * Retrieves one assetMaster that is latest version linked to given originId
   *
   * @param id originId
   * @return `Future[Some(AssetMaster)]` if available, otherwise `Future[None]`
   */
  def resolveLatestByOriginId(id: OriginId): Future[Option[AssetMaster]]

  /**
   * Resolves a map of `AssetMaster` entities (that is latest version) associated `OriginId`.
   *
   * @param ids A set of `OriginId`
   * @return A `Future[Map[OriginId, Seq[AssetMaster]]]`. Always returns all input origins as keys
   */
  def resolveLatestByOriginIds(originIds: Set[OriginId]): Future[Map[OriginId, Seq[AssetMaster]]]

  /**
   * This method is useful for retrieving a specific version of an `AssetMaster`
   * This allows for handling scenarios where the specified version of the `AssetMaster` does not exist.
   *
   * @param id The unique identifier of the `AssetMaster` to be resolved.
   * @param version The specific version of the `AssetMaster` to retrieve.
   * @return A `Future[Some[AssetMaster]]` that contains the `AssetMaster` entity if found, otherwise `None`.
   */
  def resolveByIdAndVer(id: AssetMasterId, version: Int): Future[Option[AssetMaster]]
}
