package com.uet.atx.application.port.output.asset

import com.uet.common.model.ddd.OutputPort

trait AssetStorageRepository extends OutputPort {}
