package com.uet.atx.application.port.output.creative

import com.uet.atx.domain.advertising.creative.master.CreativeMaster
import com.uet.atx.domain.advertising.text.master.TextMasterId

import scala.concurrent.Future

trait CreativeMasterRepository {
  def resolveByTextMasterIds(textIds: Set[TextMasterId]): Future[Map[TextMasterId, Seq[CreativeMaster]]]
}
