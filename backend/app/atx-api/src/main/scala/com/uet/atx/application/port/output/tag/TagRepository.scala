package com.uet.atx.application.port.output.tag

import com.uet.atx.domain.advertising.tag.{ Tag, TagId, TagTypeId }
import com.uet.common.model.ddd.OutputPort

import scala.concurrent.Future

trait TagRepository extends OutputPort {

  /**
   * Resolves a sequence of `Tag` entities by their identifiers.
   *
   * @param ids A sequence of unique identifiers for the `Tag` entities to be resolved.
   * @return The method returns a tuple of: (found Tags, notFound tagIds)
   */
  def resolveByIds(ids: Set[TagId]): Future[(Seq[Tag], Set[TagId])]

  /**
   * Deletes `Tag` entities identified by their unique identifiers.
   * No throw exception if id does not exist
   *
   * @param ids A set of TagIds
   * @return A `Future[Unit]` for success case
   */
  def deleteByIds(ids: Set[TagId]): Future[Unit]

  /**
   * Store an `Tag` to storage. If it exists, update it and otherwise create
   *
   * @param tag The `Tag` entity to be stored.
   * @return A `Future[Unit]` if the entity is stored successfully
   */
  def store(tag: Tag): Future[Unit]

  /**
   * Resolves a `Tag` entity by its identifier.
   * This method attempts to find a `Tag` entity corresponding to the provided identifier.
   *
   * @param id The unique identifier of the `Tag` entity to be resolved.
   * @return A `Future[Some(Tag)]` If the entity is found and otherwise `Future[None]`
   */
  def resolveById(id: TagId): Future[Option[Tag]]

  /**
   * This method is designed to find all `Tag` entities that are associated with the specified TagType identifiers.
   * This method is always return all input ids as map-key in result
   *
   * @param ids A set of `TagTypeId`
   * @return A `Future[Map[TagTypeId, Seq[Tag]]]`
   */
  def resolveByTagTypeIds(ids: Set[TagTypeId]): Future[Map[TagTypeId, Seq[Tag]]]
}
