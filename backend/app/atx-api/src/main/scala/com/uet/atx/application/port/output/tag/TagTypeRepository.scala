package com.uet.atx.application.port.output.tag

import com.uet.atx.domain.advertising.tag.{ TagType, TagTypeId }
import com.uet.atx.domain.shared.PromotionId
import com.uet.common.model.ddd.OutputPort

import scala.concurrent.Future

trait TagTypeRepository extends OutputPort {

  /**
   * Store an `TagType` to storage. If it exists, update it and otherwise create
   *
   * @param entity The `TagType` entity to be stored.
   * @return A `Future[Unit]` if the entity is stored successfully
   */
  def store(entity: TagType): Future[Unit]

  /**
   * Resolves a mapping of `PromotionId` to a sequence of `TagType` entities associated with each promotion.
   *
   * @param ids A set of `PromotionId` for which the tag types are to be resolved.
   * @return A `Future[Map[OriginId, Seq[TagType]]]`. Always returns all input promotionIds as keys
   */
  def resolveByPromotionIds(ids: Set[PromotionId]): Future[Map[PromotionId, Seq[TagType]]]

  /**
   * Resolves a sequence of `TagType` entities by their identifiers.
   *
   * @param ids A sequence of unique identifiers for the `TagType` entities to be resolved.
   * @return The method returns a tuple of: (found TagTypes, notFound TagTypeIds)
   */
  def resolveByIds(ids: Set[TagTypeId]): Future[(Seq[TagType], Set[TagTypeId])]

  /**
   * Resolves a `TagType` entity by its identifier.
   * This method attempts to find a `TagType` entity corresponding to the provided identifier.
   *
   * @param id The unique identifier of the `TagType` entity to be resolved.
   * @return A `Future[Some(TagType)]` If the entity is found and otherwise `Future[None]`
   */
  def resolveById(id: TagTypeId): Future[Option[TagType]]
}
