package com.uet.atx.application.service

import com.uet.atx.application.port.input.employee.{ EmployeeDTO, EmployeeUsecase }
import com.uet.atx.application.port.output.EmployeeRepository
import com.uet.common.fn.Concurrency.EC
import com.uet.common.model.ddd.Email

import scala.concurrent.Future

private[application] class EmployeeService(employeeRepository: EmployeeRepository)(implicit val ec: EC) extends EmployeeUsecase {

  /**
   * Retrieves a list of all employees
   */
  override def retrieveAllEmployees: Future[Seq[EmployeeDTO.Result]] =
    employeeRepository
      .resolveAll()
      .map(_.map(new EmployeeDTO.Result(_)))

  /**
   * Retrieves an employee by their email.
   *
   * @param email The email of the employee to be retrieved.
   * @return A Future containing the EmployeeDTO of the requested employee if found.
   * @throws NoSuchElementException if no employee with the given email is found
   */
  override def retrieveByEmail(email: Email): Future[EmployeeDTO.Result] = {
    employeeRepository
      .resolveByEmail(email)
      .map {
        case Some(employee) => new EmployeeDTO.Result(employee)
        case _              => throw new NoSuchElementException("No employee with the given email is found")
      }
  }
}
