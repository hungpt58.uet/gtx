package com.uet.atx.application.service.asset

import com.uet.atx.application.port.input.asset.AssetMasterUsecase
import com.uet.atx.application.port.input.asset.dtos.{ AMSearchDTO, AssetMasterDTO }
import com.uet.atx.application.port.output.LabelRepository
import com.uet.atx.application.port.output.asset.AssetMasterRepository
import com.uet.atx.application.port.output.tag.{ TagRepository, TagTypeRepository }
import com.uet.atx.domain.advertising.asset.AssetMasterId
import com.uet.common.fn.Concurrency.EC

import scala.concurrent.Future

private[application] class AssetMasterService(
  labelRepository:       LabelRepository,
  tagRepository:         TagRepository,
  tagTypeRepository:     TagTypeRepository,
  assetMasterRepository: AssetMasterRepository
)(implicit ec: EC) extends BaseAMService(tagRepository, tagTypeRepository) with AssetMasterUsecase {

  /**
   * Searches for asset masters based on the given search criteria.
   *
   * @param criteria The search criteria encapsulated in `AMSearchDTO`.
   * @return A `Future` containing a sequence of `AssetMasterDTO` that match the search criteria.
   */
  override def search(criteria: AMSearchDTO): Future[Seq[AssetMasterDTO]] = ???

  /**
   * Retrieves an asset master by its raw asset ID.
   *
   * @param rawAssetId The ID of the asset to retrieve.
   * @return A `Future` containing the `AssetMasterDTO` corresponding to the specified raw asset ID.
   * @throws NoSuchElementException if don't find any asset master
   */
  override def get(rawAssetId: String): Future[AssetMasterDTO] = {
    val typedAssetId = AssetMasterId(rawAssetId)

    assetMasterRepository
      .resolveById(typedAssetId)
      .flatMap {
        case Some(am) => makeMultiAMWithTag(Seq(am)).map(_.head) // _.head is always right
        case None     => throw new NoSuchElementException(s"Not found AssetMaster with id: $rawAssetId")
      }
  }
}
