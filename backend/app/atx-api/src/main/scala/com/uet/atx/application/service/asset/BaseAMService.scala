package com.uet.atx.application.service.asset

import com.uet.atx.application.port.input.asset.dtos.AssetMasterDTO
import com.uet.atx.application.port.output.tag.{ TagRepository, TagTypeRepository }
import com.uet.atx.domain.advertising.asset.AssetMaster
import play.api.Logging

import scala.concurrent.Future

private[asset] abstract class BaseAMService(
  tagRepository:     TagRepository,
  tagTypeRepository: TagTypeRepository
) extends Logging {

  protected def makeMultiAMWithTag(assetMasters: Seq[AssetMaster]): Future[Seq[AssetMasterDTO]] = ???
}
