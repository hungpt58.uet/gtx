package com.uet.atx.application.service.tag

import com.uet.atx.application.port.input.tag.{ TagDTO, TagUsecase }
import com.uet.atx.application.port.output.tag.{ TagRepository, TagTypeRepository }
import com.uet.atx.domain.advertising.tag.{ Tag, TagId }
import com.uet.common.fn.Concurrency.EC

import scala.concurrent.Future

private[application] class TagService(
  tagRepository:     TagRepository,
  tagTypeRepository: TagTypeRepository
)(implicit ec: EC) extends TagUsecase {

  /**
   * Stores a new Tag entity based on the provided DTO.
   *
   * @param dto contains the details of the Tag to be created.
   * @return A Future of new Tag
   * @throws NoSuchElementException if tagTypeId doesn't exist
   */
  override def createNew(dto: TagDTO.Create): Future[TagDTO.Result] = {
    tagTypeRepository
      .resolveById(dto.tagTypeId)
      .flatMap {
        case None => throw new NoSuchElementException(s"Not found TagType entity with id: ${dto.tagTypeId}")
        case _    =>
          val entity = dto.toEntity
          tagRepository.store(entity).map(_ => new TagDTO.Result(entity))
      }
  }

  /**
   * Deletes the Tags identified by the given set of TagIds.
   *
   * @param ids a Set of TagId
   * @return A Future[Unit] in case successful
   * @throws NoSuchElementException if tagIds didn't find
   */
  override def delete(ids: Set[TagId]): Future[Unit] = {
    tagRepository
      .resolveByIds(ids)
      .flatMap {
        case (_, noExistIds) if noExistIds.nonEmpty =>
          throw new NoSuchElementException(s"Not found Tag entities with ids: [${noExistIds.mkString(", ")}]")

        case (_, _) => tagRepository.deleteByIds(ids)
      }
  }

  /**
   * Updates an existing Tag entity based on the provided DTO.
   *
   * @param dto contains the updated details of the Tag.
   * @return A Future containing the updated Tag result.
   * @throws NoSuchElementException if the tagId or tagTypeId don't exist.
   */
  override def update(dto: TagDTO.Update): Future[TagDTO.Result] = {
    val f1 = tagTypeRepository.resolveById(dto.tagTypeId).map {
      case None          => throw new NoSuchElementException(s"Not found TagType entity with id: ${dto.tagTypeId}")
      case Some(tagType) => tagType
    }
    val f2 = tagRepository.resolveById(dto.id).map {
      case None      => throw new NoSuchElementException(s"Not found Tag entity with id: ${dto.id}")
      case Some(tag) => tag
    }

    (for (_ <- f1; _ <- f2) yield {
      val newEntity = dto.toEntity

      tagRepository
        .store(newEntity)
        .map(_ => new TagDTO.Result(newEntity))
    }).flatten
  }

  /**
   * Searches for Tags based on the criteria provided in the DTO.
   *
   * @param dto the search criteria encapsulated in a TagDTO.Search object.
   * @return A Future containing a sequence of TagDTO.Result, representing the search results.
   */
  override def search(dto: TagDTO.Search): Future[Seq[TagDTO.Result]] = {
    // -> INNER FUNCTION
    val filterByTagTypeId = (tags: Seq[Tag]) => {
      dto.tagTypeId match {
        case Some(tagTypeId) => tags.filter(_.tagTypeId == tagTypeId)
        case None            => tags
      }
    }

    val filterByName = (tags: Seq[Tag]) => {
      dto.name match {
        case Some(name) => tags.filter(_.name == name)
        case None       => tags
      }
    }

    // -> START HERE
    tagTypeRepository
      .resolveByPromotionIds(dto.promotionIds)
      .map(_.values.flatten.map(_.id).toSet) // Set of TagTypeId
      .flatMap(tagRepository.resolveByTagTypeIds)
      .map(_.values.flatten.toSeq)           // Seq of Tag
      .map(filterByTagTypeId)
      .map(filterByName)
      .map(_.map(new TagDTO.Result(_)))
  }
}
