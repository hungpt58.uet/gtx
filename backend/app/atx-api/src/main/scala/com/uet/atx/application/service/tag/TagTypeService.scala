package com.uet.atx.application.service.tag

import com.uet.atx.application.port.input.tag.{ TagTypeDTO, TagTypeUsecase }
import com.uet.atx.application.port.output.tag.{ TagRepository, TagTypeRepository }
import com.uet.atx.domain.advertising.tag.TagType
import com.uet.common.fn.Concurrency.EC

import scala.concurrent.Future

private[application] class TagTypeService(
  tagTypeRepository: TagTypeRepository,
  tagRepository:     TagRepository
)(implicit ec: EC) extends TagTypeUsecase {

  /**
   * Stores a new TagType entity based on the provided DTO.
   *
   * @param dto contains the details of the TagType to be created.
   * @return A Future of new TagTypeEntity
   */
  override def createNew(dto: TagTypeDTO.Create): Future[TagTypeDTO.Result] = {
    val entity = dto.toEntity

    tagTypeRepository
      .store(entity)
      .map(_ => new TagTypeDTO.Result(entity))
  }

  /**
   * Searches for TagType entities based on the criteria provided in the DTO.
   *
   * @param dto contains the search criteria for TagTypes.
   * @return A Future containing a sequence of TagTypeDTO.Result, representing the search results.
   */
  override def search(dto: TagTypeDTO.Search): Future[Seq[TagTypeDTO.Result]] = {
    // -> INNER FUNCTION
    def filterByTagIds: Future[Option[Seq[TagType]]] = {
      if (dto.tagIds.isEmpty) {
        Future.successful(None)
      } else {
        tagRepository
          .resolveByIds(dto.tagIds)
          .map { case (tags, _) => tags.map(_.tagTypeId).toSet } // Set[TagTypeId]
          .flatMap(tagTypeRepository.resolveByIds) // (Seq[TagType], Set[TagTypeId])
          .map { case (tagTypes, _) => Some(tagTypes) } // Some(Seq[TagType])
      }
    }

    def filterByPromotionIds: Future[Option[Seq[TagType]]] = {
      if (dto.promotionIds.isEmpty) {
        Future.successful(None)
      } else {
        tagTypeRepository
          .resolveByPromotionIds(dto.promotionIds) // Map[PromotionId, Seq[TagType]]
          .map(_.values.flatten.toSeq)             // Seq[TagType]
          .map(Some(_))                            // Some(Seq[TagType])
      }
    }

    // -> START HERE
    val f1 = filterByTagIds
    val f2 = filterByPromotionIds

    for { result01 <- f1; result02 <- f2 } yield {
      Seq(result01, result02)
        .collect { case Some(tagTypes) => tagTypes }
        .foldLeft(Seq.empty[TagType])(_.intersect(_))
        .map(new TagTypeDTO.Result(_))
    }
  }
}
