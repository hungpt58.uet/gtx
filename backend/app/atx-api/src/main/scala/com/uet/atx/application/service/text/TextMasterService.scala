package com.uet.atx.application.service.text

import com.uet.atx.application.port.input.text.{ TextDTO, TextMasterUsecase }
import com.uet.atx.application.port.output.creative.CreativeMasterRepository
import com.uet.atx.application.port.output.tag.{ TagRepository, TagTypeRepository }
import com.uet.atx.application.port.output.{ LabelRepository, RuleRepository, TextMasterRepository }
import com.uet.atx.domain.advertising.text.master.{ TextMaster, TextMasterId }
import com.uet.atx.domain.shared.{ AdFormatCheckType, ApprovalStatus, MasterStatus }
import com.uet.common.fn.Concurrency.EC
import com.uet.common.fn.IDGenerator
import com.uet.common.model.utilities.{ EventTime, Version }
import org.joda.time.DateTime

import scala.concurrent.Future

private[application] class TextMasterService(
  labelRepository:          LabelRepository,
  tagRepository:            TagRepository,
  tagTypeRepository:        TagTypeRepository,
  ruleRepository:           RuleRepository,
  textMasterRepository:     TextMasterRepository,
  creativeMasterRepository: CreativeMasterRepository
)(implicit ec: EC) extends TextMasterUsecase {

  /**
   * Creates a new TextMaster entity based on the provided DTO
   * This method first checks if the text adheres to predefined rules and if a similar TextMaster already exists.
   * If the text passes the checks and no similar TextMaster is found, a new TextMaster is created and stored.
   *
   * @param dto containing the necessary information to create a new TextMaster.
   * @return A Future containing the result DTO of the newly created or found existing TextMaster.
   * @throws IllegalStateException If the text does not pass the predefined rules.
   * @throws IllegalArgumentException when having id is not found. Id can be labelId, tagId or tagTypeId
   */
  override def createNew(dto: TextDTO.Create): Future[TextDTO.Result] = {
    val f1 = ruleRepository.checkText(Some(dto.promotionId), dto.labeledTexts, dto.adFormatId, dto.checkType)
    val f2 = textMasterRepository.resolveLatestByPromotionId(dto.promotionId)

    (for (checkedResult <- f1; existingTMasters <- f2) yield {
      val isError = checkedResult.nonEmpty
      if (isError) throw new IllegalStateException(checkedResult.mkString("-"))

      existingTMasters.find(_.isSameOtherLabeledTexts(dto.labeledTexts)) match {
        case Some(tm) => // Found case
          buildResultWithTag(Seq(tm)).map(_.head) // always have one element at first

        case None => // No exist case
          val newTextMaster =
            TextMaster(
              id           = TextMasterId(IDGenerator.nextId),
              promotionId  = dto.promotionId,
              adFormatId   = dto.adFormatId,
              labeledTexts = dto.labeledTexts,
              projectName  = dto.projectName,
              comment      = dto.comment,
              score        = dto.score,
              status       = new MasterStatus(),
              tagIds       = Set.empty,
              checkType    = dto.checkType.getOrElse(AdFormatCheckType.Normal),
              version      = Version.first,
              eventTime    = EventTime(DateTime.now(), None)
            )

          textMasterRepository
            .store(newTextMaster)
            .flatMap(_ => buildResultWithTag(Seq(newTextMaster)))
            .map(_.head) // always have one element at first
      }
    }).flatten
  }

  override def update(dto: TextDTO.Update): Future[TextDTO.Result] = {
    val newTMasterFuture =
      textMasterRepository
        .resolveLatestById(dto.id)
        .map {
          case None             => throw new IllegalArgumentException(s"Not found text master by id: ${dto.id}")
          case Some(oldTMaster) =>
            var newTMaster = oldTMaster.copy(
              comment     = dto.comment,
              projectName = dto.projectName,
              score       = dto.score,
              adFormatId  = dto.adFormatId,
              checkType   = dto.checkType.getOrElse(oldTMaster.checkType)
            )

            val isSameLabeledTxt = oldTMaster.isSameOtherLabeledTexts(dto.labeledTexts)
            if (!isSameLabeledTxt) {
              newTMaster = newTMaster.copy(
                labeledTexts = dto.labeledTexts,
                status       = new MasterStatus(),
                version      = oldTMaster.version.upVersion
              )
            }

            newTMaster
        }

    val checkedResultFuture = newTMasterFuture.flatMap {
      newTMaster =>
        ruleRepository.checkText(
          Some(newTMaster.promotionId),
          newTMaster.labeledTexts,
          newTMaster.adFormatId,
          Some(newTMaster.checkType)
        )
    }

    val oldTMastersFuture = newTMasterFuture.flatMap {
      newTMaster => textMasterRepository.resolveLatestByPromotionId(newTMaster.promotionId)
    }

    (for {
      newTMaster       <- newTMasterFuture
      checkedResult    <- checkedResultFuture
      existingTMasters <- oldTMastersFuture
    } yield {
      val isError = checkedResult.nonEmpty
      if (isError) throw new IllegalStateException(checkedResult.mkString("-"))

      existingTMasters.find(_.isSameOtherLabeledTexts(newTMaster.labeledTexts)) match {
        case Some(tm) => // Found case
          buildResultWithTag(Seq(tm)).map(_.head) // always have one element at first

        case None => // No exist case
          textMasterRepository
            .store(newTMaster)
            .flatMap(_ => buildResultWithTag(Seq(newTMaster)))
            .map(_.head) // always have one element at first
      }
    }).flatten
  }

  override def retrieve(id: TextMasterId, version: Option[Int]): Future[Any] = ???

  /**
   * Deletes a set of TextMaster entities identified by their IDs.
   * This method performs several checks before proceeding with the deletion:
   * - Ensures that none of the TextMasters are currently being used in any creative.
   * - Validates the existence of all specified TextMaster IDs.
   * - Checks if any of the TextMasters are in the CustomerChecking stage, which would prevent deletion.
   *
   * If any of these checks fail, an appropriate exception is thrown.
   * If all checks pass, the method proceeds to mark the TextMasters as not latest (logical deletion) by setting their `isLatest` flag to false.
   *
   * @param ids A set of TextMasterId
   * @return A Future[Unit] indicating the completion of the operation
   * @throws IllegalStateException If any of the TextMasters are being used in creatives or are in the CustomerChecking stage.
   * @throws IllegalArgumentException If any of the specified TextMaster IDs do not exist.
  */
  override def delete(ids: Set[TextMasterId]): Future[Unit] = {
    val f1 = creativeMasterRepository.resolveByTextMasterIds(ids)
    val f2 = textMasterRepository.resolveLatestByIds(ids)

    (for { creatives <- f1; texts <- f2 } yield {
      // => Validate
      if (creatives.nonEmpty) {
        throw new IllegalStateException("Cannot be deleted because it is being used creative")
      }

      texts match {
        case _ -> notFoundIds if notFoundIds.nonEmpty =>
          throw new IllegalArgumentException(s"Not found text master with ids: [${texts._2.mkString(", ")}]")

        case textMasters -> _ =>
          val isCustomerCheckingStage = textMasters.exists(_.status.approvalStatus == ApprovalStatus.CustomerChecking)
          if (isCustomerCheckingStage) {
            throw new IllegalStateException("The text is currently being verified by the customer and cannot be deleted")
          }
      }

      // => Set isLatest flag to false as delete
      val updatedTMList = texts._1.map { tm =>
        val updatedVersion = tm.version.copy(isLatest = false)
        tm.copy(version = updatedVersion)
      }
      textMasterRepository.store(updatedTMList)
    }).flatten
  }

  override def search(dto: TextDTO.Search): Future[Any] = ???

  override def retrieveLatest(dto: TextDTO.Latest): Future[Any] = ???

  override def checkValidityByRule(dto: TextDTO.Rule): Future[Any] = ???

  private def buildResultWithTag(textMasters: Seq[TextMaster]): Future[Seq[TextDTO.Result]] = {
    val tmFuture = Future.successful(textMasters)

    val f1 =
      tmFuture
        .map(_.flatMap(_.labeledTexts)) // Future[Seq[LabeledText]]
        .map(_.map(_.labelId).toSet)    // Future[Set[LabelId]]
        .flatMap(labelRepository.resolveByIds)
        .map {
          case _ -> notFoundIds if notFoundIds.nonEmpty =>
            throw new IllegalArgumentException(s"Not found label with ids: [${notFoundIds.mkString(", ")}]")
          case foundLabels -> _                         =>
            foundLabels
        }

    val f2 =
      tmFuture
        .map(_.flatMap(_.tagIds).toSet) // Future[Set[TagId]]
        .flatMap(tagRepository.resolveByIds)
        .map {
          case _ -> notFoundIds if notFoundIds.nonEmpty =>
            throw new IllegalArgumentException(s"Not found tag with ids: [${notFoundIds.mkString(", ")}]")
          case foundTags -> _                           =>
            foundTags
        }

    val f3 =
      f2.map(_.map(_.tagTypeId).toSet) // Future[Set[TagTypeId]]
        .flatMap(tagTypeRepository.resolveByIds)
        .map {
          case _ -> notFoundIds if notFoundIds.nonEmpty =>
            throw new IllegalArgumentException(s"Not found tag type with ids: [${notFoundIds.mkString(", ")}]")
          case foundTagTypes -> _                       =>
            foundTagTypes
        }

    for {
      labels   <- f1
      tags     <- f2
      tagTypes <- f3
    } yield TextDTO.ofResults(textMasters, labels, tags, tagTypes)
  }
}
