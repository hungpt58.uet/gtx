package com.uet.atx.domain.advertising.asset

import com.uet.atx.domain.advertising.asset.properties.{ AssetItem, IdGroup }
import com.uet.atx.domain.advertising.tag.TagId
import com.uet.atx.domain.shared.{ ApprovalStatus, MasterStatus, SubmitStatus }
import com.uet.common.model.ddd.AggregateRoot
import com.uet.common.model.pattern.MyEnum
import com.uet.common.model.utilities.{ EventTime, Version }

case class AssetMaster(
  id:           AssetMasterId,
  idGroup:      IdGroup,
  item:         AssetItem,
  status:       MasterStatus,
  projectNames: Set[String],
  comment:      Option[String],
  tagIds:       Set[TagId],
  version:      Version,
  eventTime:    EventTime
) extends AggregateRoot[AssetMasterId, AssetMaster] {

  def setNoApprovedStatus(reason: String): AssetMaster = {
    val newNoApprovedStatus = ApprovalStatus.NotApproved(Set(reason))
    val updatedStatus       = this.status.copy(
      approvalStatus = newNoApprovedStatus.updateWith(this.status.approvalStatus)
    )

    this.copy(status = updatedStatus)
  }

  def setStatus[T <: MyEnum](status: MyEnum): AssetMaster = {
    status match {
      case as: ApprovalStatus => this.copy(status = this.status.copy(approvalStatus = as))
      case ss: SubmitStatus   => this.copy(status = this.status.copy(submitStatus = ss))
      case _                  => this
    }
  }

  def isApprovedAndContainTextInRawFileName(text: String): Boolean = {
    val isApprovedStatus = this.status.approvalStatus == ApprovalStatus.Approved()
    val containsText     = this.item.originalFileName.contains(text)

    isApprovedStatus && containsText
  }
}
