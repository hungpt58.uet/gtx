package com.uet.atx.domain.advertising.asset

import com.uet.common.model.ddd.Identifier

case class AssetMasterId(value: String) extends Identifier[String]
