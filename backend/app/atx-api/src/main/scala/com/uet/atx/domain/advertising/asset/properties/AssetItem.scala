package com.uet.atx.domain.advertising.asset.properties

import com.uet.atx.domain.advertising.asset.properties.Metadata.{ ImageMetadata, MusicMetadata, VideoMetadata }

case class AssetItem(
  originalFileName: String,
  aliasFileName:    Option[String],
  ctxFileName:      String,
  ctxAliasFileName: Option[String],
  assetURL:         AssetURL,
  metadata:         Option[Metadata]
) {

  def getType: AssetType = {
    metadata match {
      case Some(_: VideoMetadata) => AssetType.Video
      case Some(_: MusicMetadata) => AssetType.Music
      case Some(_: ImageMetadata) => AssetType.Image
      case None                   => AssetType.Url

      case _ => throw new UnsupportedOperationException("Not supported asset item type")
    }
  }
}
