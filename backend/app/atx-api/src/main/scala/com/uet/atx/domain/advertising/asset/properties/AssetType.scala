package com.uet.atx.domain.advertising.asset.properties

sealed abstract class AssetType(val code: Int)

object AssetType {

  case object Video extends AssetType(0)
  case object Image extends AssetType(1)
  case object Url   extends AssetType(2)
  case object Music extends AssetType(3)

  def apply(code: Int): AssetType =
    code match {
      case Video.code => Video
      case Image.code => Image
      case Url.code   => Url
      case Music.code => Music

      case _ => throw new IllegalArgumentException(s"AssetType: $code is wrong")
    }
}
