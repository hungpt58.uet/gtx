package com.uet.atx.domain.advertising.asset.properties

import java.net.URL

abstract class AssetURL

case class YTBAssetURL(youtubeURL: URL)                                    extends AssetURL
case class NormalAssetURL(thumbnailUrl: URL, storedItemURL: URL)           extends AssetURL
case class PsdAssetURL(thumbnailUrl: URL, storedItemURL: URL, psdUrl: URL) extends AssetURL
