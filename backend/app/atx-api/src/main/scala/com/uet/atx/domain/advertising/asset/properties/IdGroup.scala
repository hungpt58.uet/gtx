package com.uet.atx.domain.advertising.asset.properties

import com.uet.atx.domain.shared.{ AdflowId, AxisId, OriginId, PromotionId, SlotId }

/**
 * An OriginId links multiple assetMasters that have many versions
 * An OriginIds only link one assetMaster that is latest version
 * Ex:
 * AssetMaster A -> OriginId X at version 1
 * AssetMaster B -> OriginId X at version 2 (B is up-version of A)
 * AssetMaster C -> OriginId X at version 3 (C is up-version of B)
 * Version 3 is latest
 */
case class IdGroup(
  slotId:      Option[SlotId],
  originId:    OriginId,
  adflowId:    Option[AdflowId],
  axisId:      Option[AxisId],
  promotionId: PromotionId
)
