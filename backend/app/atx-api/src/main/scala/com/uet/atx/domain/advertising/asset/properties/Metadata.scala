package com.uet.atx.domain.advertising.asset.properties

abstract class Metadata(
  width:     Int,
  height:    Int,
  fileSize:  Long,
  extension: String,
  hashValue: String
)

object Metadata {

  case class ImageMetadata(
    width:     Int,
    height:    Int,
    fileSize:  Long,
    extension: String,
    hashValue: String
  ) extends Metadata(width, height, fileSize, extension, hashValue)

  case class MusicMetadata(
    width:       Int,
    height:      Int,
    fileSize:    Long,
    extension:   String,
    duration:    Double,
    audioVolume: Double,
    audioCodec:  String,
    hashValue:   String
  ) extends Metadata(width, height, fileSize, extension, hashValue)

  case class VideoMetadata(
    width:       Int,
    height:      Int,
    fileSize:    Long,
    extension:   String,
    duration:    Double,
    audioVolume: Double,
    audioCodec:  String,
    videoCodec:  String,
    frameRate:   Double,
    bitRate:     Double,
    hashValue:   String,
    xmpTitle:    Option[String]
  ) extends Metadata(width, height, fileSize, extension, hashValue)
}
