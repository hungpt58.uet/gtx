package com.uet.atx.domain.advertising.creative

import com.uet.common.model.ddd.{ Entity, Identifier }

trait Creative[ID <: Identifier[String]] extends Entity[ID] {
  val id: ID
}
