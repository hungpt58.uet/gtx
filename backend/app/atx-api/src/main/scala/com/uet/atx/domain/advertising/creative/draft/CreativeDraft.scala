package com.uet.atx.domain.advertising.creative.draft

import com.uet.atx.domain.advertising.creative.Creative

case class CreativeDraft(id: CreativeDraftId, content: String) extends Creative[CreativeDraftId]
