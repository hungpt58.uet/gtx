package com.uet.atx.domain.advertising.creative.draft

import com.uet.common.model.ddd.Identifier

case class CreativeDraftId(value: String) extends Identifier[String]
