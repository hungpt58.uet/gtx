package com.uet.atx.domain.advertising.creative.master

import com.uet.atx.domain.advertising.creative.Creative
import com.uet.atx.domain.advertising.tag.TagId
import com.uet.atx.domain.advertising.text.master.TextMasterId
import com.uet.atx.domain.shared.{ AdFormatCheckType, AdFormatId, MasterStatus, PromotionId }
import com.uet.common.model.utilities.EventTime

import java.net.URL

case class CreativeMaster(
  id:            CreativeMasterId,
  promotionId:   PromotionId,
  adFormatId:    AdFormatId,
  textId:        Option[TextMasterId],
  labeledAssets: Seq[LabeledAsset],
  isFixed:       Boolean,
  status:        MasterStatus,
  referenceUrl:  Option[URL],
  checkType:     AdFormatCheckType,
  tagIds:        Seq[TagId],
  comment:       Option[String],
  note:          Option[String],
  eventTime:     EventTime
) extends Creative[CreativeMasterId]
