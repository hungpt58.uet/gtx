package com.uet.atx.domain.advertising.creative.master

import com.uet.common.model.ddd.Identifier

case class CreativeMasterId(value: String) extends Identifier[String]
