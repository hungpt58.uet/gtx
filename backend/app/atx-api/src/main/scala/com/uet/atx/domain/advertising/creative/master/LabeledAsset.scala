package com.uet.atx.domain.advertising.creative.master

import com.uet.atx.domain.advertising.asset.AssetMasterId
import com.uet.atx.domain.advertising.label.LabelId
import com.uet.atx.domain.shared.OriginId
import com.uet.common.model.ddd.ValueObject

abstract class LabeledAsset(labelId: LabelId) extends ValueObject

case class LatestLabeledAsset(labelId: LabelId, originIds: Set[OriginId]) extends LabeledAsset(labelId)

case class FixedLabeledAsset(labelId: LabelId, assetIds: Set[AssetMasterId]) extends LabeledAsset(labelId)
