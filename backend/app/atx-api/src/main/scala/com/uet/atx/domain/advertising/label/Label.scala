package com.uet.atx.domain.advertising.label

import com.uet.atx.domain.shared.AdFormatId
import com.uet.common.model.ddd.Entity

case class Label(
  id:         LabelId,
  index:      Option[Int],
  adFormatId: AdFormatId,
  name:       String,
  `type`:     String,
  memo:       Option[String]
) extends Entity[LabelId]
