package com.uet.atx.domain.advertising.label

import com.uet.common.model.ddd.Identifier

case class LabelId(value: String) extends Identifier[String]
