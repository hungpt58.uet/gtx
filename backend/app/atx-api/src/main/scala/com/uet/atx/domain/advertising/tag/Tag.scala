package com.uet.atx.domain.advertising.tag

import com.uet.common.model.ddd.Entity

case class Tag(
  id:          TagId,
  name:        String,
  tagTypeId:   TagTypeId,
  description: Option[String],
  color:       Option[String],
  isDefault:   Boolean
) extends Entity[TagId]
