package com.uet.atx.domain.advertising.tag

import com.uet.atx.domain.shared.PromotionId
import com.uet.common.model.ddd.Entity

case class TagType(
  id:          TagTypeId,
  name:        String,
  promotionId: Option[PromotionId]
) extends Entity[TagTypeId]
