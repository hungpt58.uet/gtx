package com.uet.atx.domain.advertising.tag

import com.uet.common.model.ddd.Identifier

case class TagTypeId(value: String) extends Identifier[String]
