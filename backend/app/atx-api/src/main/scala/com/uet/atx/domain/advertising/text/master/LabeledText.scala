package com.uet.atx.domain.advertising.text.master

import com.uet.atx.domain.advertising.label.LabelId
import com.uet.common.model.ddd.ValueObject

case class LabeledText(labelId: LabelId, textUnits: Set[String]) extends ValueObject {

  override def equals(obj: Any): Boolean = obj match {
    case that: LabeledText => this.labelId == that.labelId && this.textUnits == that.textUnits
    case _                 => false
  }
}
