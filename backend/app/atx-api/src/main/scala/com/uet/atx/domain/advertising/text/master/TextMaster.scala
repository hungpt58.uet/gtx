package com.uet.atx.domain.advertising.text.master

import com.uet.atx.domain.advertising.tag.TagId
import com.uet.atx.domain.shared.{ AdFormatCheckType, AdFormatId, MasterStatus, PromotionId }
import com.uet.common.model.ddd.AggregateRoot
import com.uet.common.model.utilities.{ EventTime, Version }

case class TextMaster(
  id:           TextMasterId,
  promotionId:  PromotionId,
  adFormatId:   AdFormatId,
  labeledTexts: Seq[LabeledText],
  projectName:  Option[String],
  comment:      Option[String],
  score:        Option[Double],
  status:       MasterStatus,
  checkType:    AdFormatCheckType,
  tagIds:       Set[TagId],
  version:      Version,
  eventTime:    EventTime
) extends AggregateRoot[TextMasterId, TextMaster] {

  /**
   * Compare this.labeledTexts with other labeledTexts base on:
   * - Must equal length
   * - this.labeledTexts must be nonEmpty.
   * It will not be same if both of labelTexts are empty
   *
   * @param others
   * @return
   */
  def isSameOtherLabeledTexts(others: Seq[LabeledText]): Boolean = {
    val isLengthEqual = this.labeledTexts.length == others.length

    isLengthEqual -> this.labeledTexts.isEmpty match {
      case (true, false) => this.labeledTexts.forall(others.contains)
      case _             => false
    }
  }

  override def equals(obj: Any): Boolean = obj match {
    case other: TextMaster =>
      (this.id == other.id && this.version == other.version) ||
      (this.isSameOtherLabeledTexts(other.labeledTexts) && this.promotionId == other.promotionId)
    case _                 => false
  }
}
