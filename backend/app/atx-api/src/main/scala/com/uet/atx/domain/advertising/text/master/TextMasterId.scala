package com.uet.atx.domain.advertising.text.master

import com.uet.common.model.ddd.Identifier

case class TextMasterId(value: String) extends Identifier[String]
