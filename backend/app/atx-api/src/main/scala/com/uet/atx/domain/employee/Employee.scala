package com.uet.atx.domain.employee

import com.uet.common.model.ddd.{ AggregateRoot, Email }

case class Employee(
  id:    EmployeeId,
  email: Email,
  name:  Option[String]
) extends AggregateRoot[EmployeeId, Employee]
