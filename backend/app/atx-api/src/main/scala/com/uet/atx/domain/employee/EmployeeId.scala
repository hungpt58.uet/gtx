package com.uet.atx.domain.employee

import com.uet.common.model.ddd.Identifier

case class EmployeeId(value: String) extends Identifier[String]
