package com.uet.atx.domain.shared

sealed abstract class AdFormatCheckType(val code: Int)

object AdFormatCheckType {

  case object Normal extends AdFormatCheckType(0)
  case object Asset  extends AdFormatCheckType(1)

  def apply(code: Int): AdFormatCheckType =
    code match {
      case Normal.code => Normal
      case Asset.code  => Asset

      case _ => throw new IllegalArgumentException(s"Not supported AdFormatCheckType code: $code")
    }
}
