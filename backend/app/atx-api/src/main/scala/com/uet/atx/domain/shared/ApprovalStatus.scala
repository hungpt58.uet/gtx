package com.uet.atx.domain.shared

import com.uet.common.model.pattern.{ MyEnum, Translator }
import com.uet.common.model.pattern.MyEnum.TranslatableEnum
import org.apache.pekko.http.scaladsl.model.DateTime

sealed abstract class ApprovalStatus private (val code: Int) extends MyEnum {

  override def equals(obj: Any): Boolean = obj match {
    case other: ApprovalStatus => this.code == other.code
    case _                     => false
  }

  override def hashCode: Int = this.code.hashCode

  def updateWith(other: ApprovalStatus): ApprovalStatus = other
}

object ApprovalStatus {

  private type ASTranslator = Translator[ApprovalStatus, String]

  case class NotApproved(reasons: Set[String]) extends ApprovalStatus(1) {

    override def updateWith(other: ApprovalStatus): ApprovalStatus = other match {
      case NotApproved(otherReasons) => NotApproved(otherReasons ++ this.reasons)
      case _                         => this
    }
  }

  case class Approved(approvalAt: DateTime = DateTime.now) extends ApprovalStatus(0)
  case object Fixing                                       extends ApprovalStatus(2)
  case object WaitingForApproval                           extends ApprovalStatus(3)
  case object InternalChecking                             extends ApprovalStatus(4)
  case object InternalChecked                              extends ApprovalStatus(5)
  case object CustomerChecking                             extends ApprovalStatus(6)
  case object Checking                                     extends ApprovalStatus(7)
  case object DeliveryNG                                   extends ApprovalStatus(8)
  case object SendSubmitRequest                            extends ApprovalStatus(9)
  case object Submitted                                    extends ApprovalStatus(10)

  lazy val SUBMIT_STATUSES: Set[ApprovalStatus] = Set(Submitted, SendSubmitRequest)

  /**
   * This class allows for the localization of `ApprovalStatus` values by providing
   * a translator that can convert the status into a localized string representation.
   *
   * @param that The `ApprovalStatus` that is to be translated.
   * @param translator A `Translator` provides the logic to translate the `ApprovalStatus` into a localized string.
 */
  private case class TranslatableApprovalStatus(
    that:       ApprovalStatus,
    translator: Translator[ApprovalStatus, String]
  ) extends ApprovalStatus(that.code) with TranslatableEnum[ApprovalStatus, String]

  /**
   * Factory method to create a `ApprovalStatus` instance from various types of input values.
   * It supports creation from an integer code, a string representation, or directly from another `ApprovalStatus` instance.
   * For integer and string inputs, it translates them into their corresponding `ApprovalStatus` instances.
   * If the input is already a `ApprovalStatus` instance, it wraps it into a `TranslatableApprovalStatus` with the provided translator.
   *
   * @param value      The value to construct the `ApprovalStatus` from
   * @param approvalAt An optional `DateTime` specifying the approval time, relevant for the `Approved` status.
   * @param msg        An optional sequence of strings providing reasons, relevant for the `NotApproved` status.
   * @param translator A translator to localize the `ApprovalStatus`. Defaults to `DefaultTranslator`.
   * @tparam T The type of the value parameter, allowing for string, integer, or `ApprovalStatus` types.
   * @return An `ApprovalStatus` instance corresponding to the provided value
   * @throws IllegalArgumentException If the input value is not an `Int`, `String`, or `ApprovalStatus`
   */
  def apply[T](value: T)(
    approvalAt: Option[DateTime] = None,
    msg:        Option[Seq[String]] = None
  )(translator: ASTranslator = DefaultTranslator): ApprovalStatus = {
    // -> INNER FUNCTION
    def ofCode(code: Int): ApprovalStatus = {
      val requiredApprovalTimeEx = new IllegalArgumentException("Require approval time in Approved case")
      val invalidCodeEx          = new IllegalArgumentException(s"Invalid ApprovalStatus code: $code")

      code match {
        case 0                       => Approved(approvalAt.getOrElse(throw requiredApprovalTimeEx))
        case 1                       => NotApproved(msg.map(_.toSet).getOrElse(Set.empty))
        case Fixing.code             => Fixing
        case WaitingForApproval.code => WaitingForApproval
        case InternalChecking.code   => InternalChecking
        case InternalChecked.code    => InternalChecked
        case CustomerChecking.code   => CustomerChecking
        case Checking.code           => Checking
        case DeliveryNG.code         => DeliveryNG
        case SendSubmitRequest.code  => SendSubmitRequest
        case Submitted.code          => Submitted

        case _ => throw invalidCodeEx
      }
    }

    def ofStrValue(value: String): ApprovalStatus = {
      val invalidValueEx = new IllegalArgumentException(s"Approval Status: $value isn't supported")

      value match {
        case "承認"  => Approved()
        case "非承認" => NotApproved(msg.map(_.toSet).getOrElse(Set.empty))
        case "未承認" => WaitingForApproval

        case "社内確認中" => InternalChecking
        case "社内確認済" => InternalChecked
        case "監修中"   => CustomerChecking
        case "修正中"   => Fixing
        case "入稿済"   => Submitted
        case "承認待ち"  => Checking

        // Status: 破棄 is applied for asset/text, 配信NG and 入稿不可 are applied for creative
        case "破棄" | "入稿不可" => DeliveryNG
        case "入稿依頼済"       => SendSubmitRequest

        case _ => throw invalidValueEx
      }
    }

    // -> START_HERE
    value match {
      case str: String => TranslatableApprovalStatus(that = ofStrValue(str), translator = translator)
      case code: Int   => TranslatableApprovalStatus(that = ofCode(code), translator = translator)

      case other: TranslatableApprovalStatus => ApprovalStatus(other.that)(None, None)(translator)
      case other: ApprovalStatus             => TranslatableApprovalStatus(other, translator)
      case _                                 => throw new IllegalArgumentException(s"Invalid ApprovalStatus value: $value")
    }
  }

  /**
   * This class translates ApprovalStatus of Asset and Text into Japanese String
   */
  object DefaultTranslator extends Translator[ApprovalStatus, String] {

    override def translate(status: ApprovalStatus): String = {
      status match {
        case _: Approved        => "承認"
        case _: NotApproved     => "非承認"
        case WaitingForApproval => "未承認"
        case InternalChecking   => "社内確認中"
        case InternalChecked    => "社内確認済"
        case CustomerChecking   => "監修中"
        case Checking           => "承認待ち"
        case DeliveryNG         => "破棄"
        case Fixing             => "修正中"
        case SendSubmitRequest  => "入稿依頼済"
        case Submitted          => "入稿済"

        case v: TranslatableApprovalStatus => translate(v.that)
      }
    }
  }

  /**
   * This class translates ApprovalStatus of Creative into Japanese String
   */
  object CreativeTranslator extends Translator[ApprovalStatus, String] {

    /**
     * Translates an `ApprovalStatus` into its Japanese string representation.
     *
     * @param status The `ApprovalStatus` to be translated.
     * @return A `String` representing the Japanese translation of the provided `ApprovalStatus`.
     */
    override def translate(status: ApprovalStatus): String = {
      status match {
        case _: Approved        => "入稿可能"
        case _: NotApproved     => "非承認"
        case WaitingForApproval => "未承認"
        case InternalChecking   => "社内確認中"
        case InternalChecked    => "社内確認済"
        case CustomerChecking   => "監修中"
        case Checking           => "承認待ち"
        case DeliveryNG         => "入稿不可"
        case Fixing             => "修正中"
        case SendSubmitRequest  => "入稿依頼済"
        case Submitted          => "入稿済"

        case v: TranslatableApprovalStatus => translate(v.that)
      }
    }
  }
}
