package com.uet.atx.domain.shared

import org.apache.pekko.http.scaladsl.model.DateTime

case class MasterStatus(approvalStatus: ApprovalStatus, submitStatus: SubmitStatus) {

  def this() = this(ApprovalStatus.WaitingForApproval, SubmitStatus.WaitingForSubmit)

  def notApprovalReasons: Option[Set[String]] = approvalStatus match {
    case v: ApprovalStatus.NotApproved => Some(v.reasons)
    case _                             => None
  }

  def approvalAt: Option[DateTime] = approvalStatus match {
    case v: ApprovalStatus.Approved => Some(v.approvalAt)
    case _                          => None
  }
}
