package com.uet.atx.domain.shared

import com.uet.common.model.ddd.Identifier

case class AdflowId(value: String) extends Identifier[String]

case class AdFormatId(value: String) extends Identifier[String]

case class PromotionId(value: String) extends Identifier[String]

case class SlotId(value: String) extends Identifier[String]

case class AxisId(value: String) extends Identifier[String]

case class OriginId(value: String) extends Identifier[String]
