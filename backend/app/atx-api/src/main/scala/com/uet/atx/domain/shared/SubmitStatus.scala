package com.uet.atx.domain.shared

import com.uet.common.model.pattern.{ MyEnum, Translator }
import com.uet.common.model.pattern.MyEnum.TranslatableEnum

sealed abstract class SubmitStatus(val code: Int) extends MyEnum {

  override def equals(obj: Any): Boolean = obj match {
    case other: SubmitStatus => this.code == other.code
    case _                   => false
  }

  override def hashCode: Int = this.code.hashCode
}

object SubmitStatus {

  case object WaitingForSubmit  extends SubmitStatus(0)
  case object SendSubmitRequest extends SubmitStatus(1)
  case object Submitted         extends SubmitStatus(2)

  /**
   * This class allows for the localization of `SubmitStatus` values by providing
   * a translator that can convert the status into a localized string representation.
   *
   * @param that     The `SubmitStatus` that is to be translated.
   * @param translator A `Translator` provides the logic to translate the `SubmitStatus` into a localized string.
   */
  private case class TranslatableSubmitStatus(
    that:       SubmitStatus,
    translator: Translator[SubmitStatus, String]
  ) extends SubmitStatus(that.code) with TranslatableEnum[SubmitStatus, String]

  /**
   * Factory method to create a `SubmitStatus` instance from various types of input values.
   * It supports creation from an integer code, a string representation, or directly from another `SubmitStatus` instance.
   * For integer and string inputs, it translates them into their corresponding `SubmitStatus` instances.
   * If the input is already a `SubmitStatus` instance, it wraps it into a `TranslatableSubmitStatus` with the provided translator.
   *
   * @param value      The input value to create a `SubmitStatus` from
   * @param translator A `Translator` instance to use for translating the `SubmitStatus` into a localized string. Defaults to `DefaultTranslator`.
   * @tparam T The type of the input value, which can be `Int`, `String`, or `SubmitStatus`.
   * @return A `SubmitStatus` instance corresponding to the input value.
   * @throws IllegalArgumentException If the input value is not an `Int`, `String`, or `SubmitStatus`
   */
  def apply[T](value: T)(translator: Translator[SubmitStatus, String] = DefaultTranslator): SubmitStatus = {
    // -> INNER FUNCTION
    def ofCode(code: Int): SubmitStatus = {
      code match {
        case WaitingForSubmit.code  => WaitingForSubmit
        case SendSubmitRequest.code => SendSubmitRequest
        case Submitted.code         => Submitted

        case _ => throw new IllegalArgumentException(s"Invalid SubmitStatus code: $code")
      }
    }

    def ofStrValue(value: String): SubmitStatus = {
      value match {
        case "未入稿"   => WaitingForSubmit
        case "入稿依頼済" => SendSubmitRequest
        case "入稿済"   => Submitted

        case _ => throw new IllegalArgumentException(s"Submit Status: $value isn't supported")
      }

    }

    // START_HERE
    value match {
      case code: Int   => TranslatableSubmitStatus(that = ofCode(code), translator = translator)
      case str: String => TranslatableSubmitStatus(that = ofStrValue(str), translator = translator)

      case other: TranslatableSubmitStatus => SubmitStatus(other.that)(translator)
      case other: SubmitStatus             => TranslatableSubmitStatus(other, translator)
      case _                               => throw new IllegalArgumentException(s"Submit Status: $value isn't supported")
    }

  }

  /**
   * This class translates SubmitStatus into Japanese String
   */
  object DefaultTranslator extends Translator[SubmitStatus, String] {

    override def translate(status: SubmitStatus): String = {
      status match {
        case WaitingForSubmit  => "未入稿"
        case SendSubmitRequest => "入稿依頼済"
        case Submitted         => "入稿済"

        case v: TranslatableSubmitStatus => translate(v.that)
      }
    }
  }
}
