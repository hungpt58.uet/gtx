package com.uet.common.cache

import com.uet.common.fn.Concurrency.EC
import play.api.cache.AsyncCacheApi

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt

trait AsyncCacheCtx {

  protected val cache: AsyncCacheApi
  protected implicit val ec: EC

  protected val KEEP_TIME = 3 minutes

  private def getCached[T](key: String): Future[Seq[T]] = {
    cache.get[Seq[T]](key).map {
      case Some(v) => v
      case _       => Nil
    }
  }

  protected def withCached[T](f: String => Future[Seq[T]])(key: String): Future[Seq[T]] = {
    getCached[T](key).flatMap { data =>
      if (data.nonEmpty) Future.successful(data)
      else {
        f(key).map { data =>
          cache.set(s"$key", data, KEEP_TIME)
          data
        }
      }
    }
  }

}
