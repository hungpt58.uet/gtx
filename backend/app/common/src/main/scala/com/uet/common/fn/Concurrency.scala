package com.uet.common.fn

import scala.concurrent.{ ExecutionContext, Future }

object Concurrency {

  type EC = ExecutionContext

  def blocking[T](fn: => T)(implicit ctx: EC): Future[T] = Future {
    scala.concurrent.blocking {
      fn
    }
  }
}
