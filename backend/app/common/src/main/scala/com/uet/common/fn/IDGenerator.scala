package com.uet.common.fn

import com.softwaremill.id.pretty.{ IdPrettifier, PrettyIdGenerator }
import com.softwaremill.id.DefaultIdGenerator

import scala.util.Random

object IDGenerator {

  private val defaultRdNodeId = new Random().nextInt(30).toLong
  private val nodeId          = Option(System.getenv("NODE_ID")).map(_.toLong).getOrElse(defaultRdNodeId)
  private val generator       = new PrettyIdGenerator(new DefaultIdGenerator(workerId = nodeId), IdPrettifier.default)

  def nextId: String = generator.nextId().replace("-", "").toLowerCase
}
