package com.uet.common.fn

import com.uet.common.model.ddd.Identifier

trait IFnUtils {

  implicit def toRawValue[T](id: Identifier[T]): T                 = id.value
  implicit def toRawValue[T](id: Option[Identifier[T]]): Option[T] = id.map(_.value)
}
