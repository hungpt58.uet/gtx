package com.uet.common.model.ddd

trait Identifier[+A] extends Serializable {

  def value: A

  override def toString: String = value.toString

  override def equals(obj: Any): Boolean = obj match {
    case that: Identifier[_] => that.value.toString == this.value.toString
    case _                   => false
  }

  override def hashCode(): Int = value.hashCode()
}

trait Entity[ID <: Identifier[_]] {

  val id: ID

  override def equals(obj: Any): Boolean = obj match {
    case that: Entity[_] => that.id == this.id
    case _               => false
  }

  override def hashCode(): Int = id.hashCode()
}

trait AggregateRoot[ID <: Identifier[_], T <: AggregateRoot[ID, T]] extends Entity[ID]

trait ValueObject

trait DomainService
