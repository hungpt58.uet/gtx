package com.uet.common.model.ddd

case class Email(value: String) extends ValueObject {

  private val REGEX = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$"

  require(value.matches(REGEX), throw new IllegalArgumentException(s"$value is not a valid email"))

  override def toString: String = value
}
