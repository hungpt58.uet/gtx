package com.uet.common.model.ddd

trait Port

trait InputPort extends Port

trait OutputPort extends Port
