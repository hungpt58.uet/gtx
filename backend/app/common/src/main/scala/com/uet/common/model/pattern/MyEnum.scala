package com.uet.common.model.pattern

trait MyEnum {

  val code: Int

  def value: String = this.getClass.getSimpleName
}

object MyEnum {

  /**
 * A trait representing a translatable enum. It extends the base `Status` trait to include
 * functionality for translating enum codes into another representation.
 * 
 * This trait applies design pattern: Decorator
 *
 * @tparam T the type of the enum being translated, which must extend `Status`
 * @tparam R the result type after translation
 */
  trait TranslatableEnum[T <: MyEnum, R] extends MyEnum {

    /** The enum to be translated. */
    val that: T

    /** The translator to convert the enum of type `T` to type `R`. */
    val translator: Translator[T, R]

    override val code: Int = that.code
  }
}
