package com.uet.common.model.pattern

trait Translator[T, R] {
  def translate(input: T): R
}
