package com.uet.common.model.utilities

trait DBRecord[PK, FK] {

  val id: PK
  val fk: Option[FK]
}
