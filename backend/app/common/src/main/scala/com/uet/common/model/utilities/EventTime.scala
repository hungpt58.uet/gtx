package com.uet.common.model.utilities

import org.joda.time.DateTime

case class EventTime(createdAt: DateTime, updatedAt: Option[DateTime])
