package com.uet.common.model.utilities

sealed abstract class OSType(val value: String)

object OSType {

  case object Windows10 extends OSType("Windows 10")
  case object Mac       extends OSType("Mac")

  lazy val currentOS = OSType(System.getProperty("os.name"))

  private def apply(value: String): OSType = value match {
    case Windows10.value => Windows10
    case Mac.value       => Mac
    case _               => throw new IllegalArgumentException(s"Not supported OS type: $value")
  }
}
