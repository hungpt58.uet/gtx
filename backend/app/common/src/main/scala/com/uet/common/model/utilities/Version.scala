package com.uet.common.model.utilities

case class Version(isLatest: Boolean, version: Int) {
  def upVersion: Version = this.copy(version = version + 1)
}

object Version {
  val first: Version = Version(isLatest = true, 1)
}
