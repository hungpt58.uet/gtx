import sbt._
import sbt.Keys._
import Settings._
import Dependencies._

//=============/ SECTION: COMMON INFO /=============//
ThisBuild / version      := "v2.0.0"
ThisBuild / scalaVersion := "2.13.13"
ThisBuild / startYear    := Some(2024)
ThisBuild / organization := "com.uet.storm"

//=============/ SECTION: MODULES /=============//
lazy val atx = (project in file("app/atx-api"))
  .enablePlugins(PlayScala, SbtWeb)
  .disablePlugins(PlayLayoutPlugin)
  .dependsOn(common)
  .settings(commonSettings *)
  .settings(
    name                       := "atx-api",
    assembly / assemblyJarName := "atx-api.jar",
    libraryDependencies ++= atxDependencies
  )

lazy val btx = (project in file("app/btx-api"))
  .enablePlugins(PlayScala, SbtWeb)
  .disablePlugins(PlayLayoutPlugin)
  .settings(commonSettings *)
  .dependsOn(common)
  .settings(
    name                       := "btx-api",
    assembly / assemblyJarName := "btx-api.jar",
    libraryDependencies ++= atxDependencies
  )

lazy val common = (project in file("app/common"))
  .settings(name := "common")
  .settings(commonSettings *)
  .settings(libraryDependencies ++= utilityDependencies)
