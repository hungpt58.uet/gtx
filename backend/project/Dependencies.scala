import play.sbt.PlayImport.cacheApi
import sbt._

object Dependencies {

  // -> For analyze assets
  private val codec  = "commons-codec"    % "commons-codec" % "1.16.1"
  private val ffmpeg = "net.bramp.ffmpeg" % "ffmpeg"        % "0.8.0"

  // -> For database
  private val quill   = "io.getquill" %% "quill-jdbc"  % "4.8.3"
  private val sqlLite = "org.xerial"   % "sqlite-jdbc" % "3.45.2.0"

  // -> Utility
  private val jsoniter = Seq(
    "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-core"   % "2.28.4",
    "com.github.plokhotnyuk.jsoniter-scala" %% "jsoniter-scala-macros" % "2.28.4" % "provided"
  )
  private val logback  = "ch.qos.logback" % "logback-classic" % "1.5.4"
  private val jodaTime = "joda-time"      % "joda-time"       % "2.10.6"
  private val idGenerator = "com.softwaremill.common" %% "id-generator"    % "1.4.0"

  private val macwireDI = Seq(
    "com.softwaremill.macwire" %% "macros"      % "2.5.9" % "provided",
    "com.softwaremill.macwire" %% "macrospekko" % "2.5.9" % "provided",
    "com.softwaremill.macwire" %% "util"        % "2.5.9",
    "com.softwaremill.macwire" %% "proxy"       % "2.5.9"
  )

  val atxDependencies: Seq[ModuleID] = Seq(codec, ffmpeg, quill, sqlLite) ++ jsoniter ++ macwireDI
  val utilityDependencies: Seq[ModuleID] = Seq(cacheApi, logback, jodaTime, idGenerator)
}
