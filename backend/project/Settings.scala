import org.scalafmt.sbt.ScalafmtPlugin.autoImport.scalafmtOnCompile
import play.sbt.PlayImport.specs2
import sbt._
import sbt.Keys._
import sbtassembly.AssemblyKeys.assemblyMergeStrategy
import sbtassembly.AssemblyPlugin.autoImport.MergeStrategy

object Settings {

  private val developer = Developer(
    id    = "300",
    name  = "Hung_PT",
    email = "hungpt58.uet@gmail.com",
    url   = new URI("https://no-page.com").toURL
  )

  private val options = Seq(
    "-encoding",
    "utf8",
    "-deprecation",
    "-feature",
    "-unchecked",
    "-Werror",
    "-Xlint",
    "-Ywarn-dead-code",
    "-Ywarn-value-discard",
    "-Xfatal-warnings",
    "-language:implicitConversions",
    "-language:higherKinds",
    "-language:existentials",
    "-language:postfixOps"
  )

  val commonSettings = Seq(
    scalacOptions ++= options,
    javaOptions ++= Seq("-Dquill.macro.log=false"),
    libraryDependencies += specs2      % Test,
    developers                        := List(developer),
    scalafmtOnCompile                 := true,
    Test / parallelExecution          := true,
    Test / fork                       := false,
    Compile / scalaSource             := baseDirectory.value / "src" / "main" / "scala",
    Compile / resourceDirectory       := baseDirectory.value / "src" / "main" / "resources",
    Test / scalaSource                := baseDirectory.value / "src" / "test" / "scala",
    Test / resourceDirectory          := baseDirectory.value / "src" / "test" / "resources",
    ThisBuild / assemblyMergeStrategy := {
      case v if v.contains("reference-overrides.conf") => MergeStrategy.first
      case v if v.contains("module-info.class")        => MergeStrategy.first
      case v                                           =>
        val oldStrategy = (ThisBuild / assemblyMergeStrategy).value
        oldStrategy(v)
    }
  )
}
