addSbtPlugin("org.playframework" % "sbt-plugin" % "3.0.2") // -> Play Framework
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "2.2.0") // -> Build Fat Jar
addSbtPlugin("org.scalameta"     % "sbt-scalafmt"  % "2.5.0") // -> Format code style